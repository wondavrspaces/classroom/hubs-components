import { paths } from "../paths";
export class KeyboardDevice {
  constructor() {
    this.seenEvents = new Map();
    this.events = [];

    ["keydown", "keyup"].map(x =>
      document.addEventListener(x, e => {
        if (!e.key) return;
        this.events.push(e);

        // Block browser hotkeys for chat command, media browser and freeze
        if (
          (e.type === "keydown" &&
            e.key === "/" &&
            !["TEXTAREA", "INPUT"].includes(document.activeElement && document.activeElement.nodeName)) ||
          e.key === "Tab" ||
          // Block browser behavior for ctrl+s and ctrl+d for now as it is not used
          (e.ctrlKey && (e.key === "s" || e.key === "d")) ||
          (e.metaKey && (e.key === "s" || e.key === "d"))
        ) {
          e.preventDefault();
          return false;
        }
      })
    );
    ["blur"].map(x => window.addEventListener(x, this.events.push.bind(this.events)));
  }

  write(frame) {
    this.events.forEach(event => {
      if (event.type === "blur") {
        this.seenEvents.clear();
        this.events.length = 0;
        return;
      }
      this.seenEvents.set(event.code, event);
    });

    this.events.length = 0;

    for (const seenEvent of this.seenEvents.values()) {
      const key = seenEvent.key;
      const code = seenEvent.code;
      const keyPath = paths.device.keyboard.key(key);
      const codePath = paths.device.keyboard.code(code);
      // On macOS, the keyup event is not received when the meta key is pressed
      // We forcefully force a keyup event in the next frame
      if (seenEvent.metaKey && key !== "Meta") {
        this.events.push(new KeyboardEvent("keyup", seenEvent));
      }
      frame.setValueType(keyPath, seenEvent.type === "keydown");
      frame.setValueType(codePath, seenEvent.type === "keydown");
    }
  }
}
