import { sets } from "../sets";
import { paths } from "../paths";
import { Pose } from "../pose";

const calculateCursorPose = function(camera, coords, origin, direction, cursorPose) {
  origin.setFromMatrixPosition(camera.matrixWorld);
  direction
    .set(coords[0], coords[1], 0.5)
    .unproject(camera)
    .sub(origin)
    .normalize();
  cursorPose.fromOriginAndDirection(origin, direction);
  return cursorPose;
};

//Check if an element or its parent are unmovable
const iterationLimit = 100;
const isUnmovable = function(elt, iteration = 0) {
  if (!elt) return false;
  if (elt.is && elt.is("unmovable")) return true;
  if (iteration > iterationLimit || !elt.parentEl) return false;
  return isUnmovable(elt.parentEl, ++iteration);
};

export class AppAwareMouseDevice {
  constructor() {
    this.prevButtonLeft = false;
    this.clickedOnAnything = false;
    this.cursorPose = new Pose();
    this.prevCursorPose = new Pose();
    this.origin = new THREE.Vector3();
    this.prevOrigin = new THREE.Vector3();
    this.direction = new THREE.Vector3();
    this.prevDirection = new THREE.Vector3();
  }

  write(frame) {
    this.prevCursorPose.copy(this.cursorPose);
    this.prevOrigin.copy(this.origin);
    this.prevDirection.copy(this.prevDirection);

    if (!this.cursorController) {
      this.cursorController = document.querySelector("[cursor-controller]").components["cursor-controller"];
    }

    if (!this.camera) {
      this.camera = document.querySelector("#player-camera").components.camera.camera;
    }

    const buttonLeft = frame.get(paths.device.mouse.buttonLeft);
    const buttonRight = frame.get(paths.device.mouse.buttonRight);
    if (buttonLeft && !this.prevButtonLeft) {
      const rawIntersections = [];
      this.cursorController.raycaster.intersectObjects(this.cursorController.targets, true, rawIntersections);
      let intersection;
      const overridingPriorityIntersection = rawIntersections.find(
        x =>
          x.object.el &&
          x.object.el.classList &&
          x.object.el.classList.contains(this.cursorController.data.overridingIntersectPriorityClass)
      );
      if (overridingPriorityIntersection) {
        intersection = overridingPriorityIntersection;
      } else {
        intersection = rawIntersections.find(x => x.object.el);
      }
      const userinput = AFRAME.scenes[0].systems.userinput;
      this.clickedOnAnything =
        (intersection &&
          intersection.object.el.matches(".pen, .pen *, .video, .video *, .interactable, .interactable *") &&
          !isUnmovable(intersection.object.el)) ||
        userinput.activeSets.has(sets.cursorHoldingPen) ||
        userinput.activeSets.has(sets.cursorHoldingInteractable) ||
        userinput.activeSets.has(sets.cursorHoldingCamera);
    }
    this.prevButtonLeft = buttonLeft;

    if (!buttonLeft) {
      this.clickedOnAnything = false;
    }

    if ((!this.clickedOnAnything && buttonLeft) || buttonRight) {
      const movementXY = frame.get(paths.device.mouse.movementXY);
      if (movementXY) {
        frame.setVector2(paths.device.smartMouse.cameraDelta, movementXY[0], movementXY[1]);
      }
    }

    const coords = frame.get(paths.device.mouse.coords);
    frame.setPose(
      paths.device.smartMouse.cursorPose,
      calculateCursorPose(this.camera, coords, this.origin, this.direction, this.cursorPose)
    );
  }
}
