import { sets } from "./sets";

let rightHandState;
let leftHandState;
let cursorHand;
let leftTeleporter;
let rightTeleporter;
let cursorController;
let penSystem;
let npcManagerSystem;
let cursorTeleport;
let vrInputSystem;
let nightModeSystem;

export function resolveActionSets() {
  rightHandState = rightHandState || document.querySelector("#player-right-controller").components["super-hands"].state;
  leftHandState = leftHandState || document.querySelector("#player-left-controller").components["super-hands"].state;
  cursorTeleport = cursorTeleport || document.querySelector("#cursor-controller").components["cursor-teleport"];
  cursorHand = cursorHand || document.querySelector("#cursor").components["super-hands"].state;
  leftTeleporter = leftTeleporter || document.querySelector("#player-left-controller").components["teleport-controls"];
  rightTeleporter =
    rightTeleporter || document.querySelector("#player-right-controller").components["teleport-controls"];
  cursorController = cursorController || document.querySelector("#cursor-controller").components["cursor-controller"];
  penSystem = penSystem || AFRAME.scenes[0].systems["pen-system"];
  npcManagerSystem = npcManagerSystem || AFRAME.scenes[0].systems["npc-manager"];
  vrInputSystem = vrInputSystem || AFRAME.scenes[0].systems["text-input"];
  nightModeSystem = nightModeSystem || AFRAME.scenes[0].systems["night-mode-manager"];
  const penActive = penSystem && penSystem.isPenActive;
  const npcActive = npcManagerSystem && npcManagerSystem.isActive() ;
  const nightModeActive = nightModeSystem && nightModeSystem.isActive;
  const leftHandHoveringOnActionable =
    !leftTeleporter.active &&
    leftHandState.has("hover-start") &&
    leftHandState.get("hover-start").matches(".actionable, .actionable *");

  const leftHandHoveringOnInteractable =
    !leftTeleporter.active &&
    !leftHandHoveringOnActionable &&
    leftHandState.has("hover-start") &&
    leftHandState.get("hover-start").matches(".interactable, .interactable *");
  const leftHandHoveringOnPen =
    !leftTeleporter.active &&
    leftHandState.has("hover-start") &&
    leftHandState.get("hover-start").matches(".pen, .pen *");
  const leftHandHoveringOnCamera =
    !leftTeleporter.active &&
    leftHandState.has("hover-start") &&
    leftHandState.get("hover-start").matches(".icamera, .icamera *");
  const leftHandHoldingInteractable =
    leftHandState.has("grab-start") && leftHandState.get("grab-start").matches(".interactable, .interactable *");
  const leftHandHoldingPen = leftHandState.has("grab-start") && leftHandState.get("grab-start").matches(".pen, .pen *");
  const leftHandHoldingCamera =
    leftHandState.has("grab-start") && leftHandState.get("grab-start").matches(".icamera, .icamera *");
  const leftHandHovering = !leftTeleporter.active && leftHandState.has("hover-start");
  const leftHandHoveringOnNothing = !leftHandHovering && !leftHandState.has("grab-start");
  const leftHandTeleporting = leftTeleporter.active;

  const cursorGrabbing = cursorHand.has("grab-start");

  const rightHandTeleporting = rightTeleporter.active;
  const rightHandHovering = !rightHandTeleporting && !cursorGrabbing && rightHandState.has("hover-start");
  const rightHandGrabbing = !rightHandTeleporting && !cursorGrabbing && rightHandState.has("grab-start");
  const rightHandHoveringOnActionable =
    !rightHandTeleporting &&
    !cursorGrabbing &&
    rightHandState.has("hover-start") &&
    rightHandState.get("hover-start").matches(".actionable, .actionable *");

  const rightHandHoveringOnInteractable =
    !rightHandTeleporting &&
    !rightHandHoveringOnActionable &&
    !cursorGrabbing &&
    rightHandState.has("hover-start") &&
    rightHandState.get("hover-start").matches(".interactable, .interactable *");
  const rightHandHoveringOnPen =
    !rightHandTeleporting &&
    !cursorGrabbing &&
    rightHandState.has("hover-start") &&
    rightHandState.get("hover-start").matches(".pen, .pen *");
  const rightHandHoveringOnCamera =
    !rightTeleporter.isTeleporting &&
    !cursorGrabbing &&
    rightHandState.has("hover-start") &&
    rightHandState.get("hover-start").matches(".icamera, .icamera *");
  const rightHandHoldingInteractable =
    !cursorGrabbing &&
    rightHandState.has("grab-start") &&
    rightHandState.get("grab-start").matches(".interactable, .interactable *");
  const rightHandHoldingPen =
    rightHandState.has("grab-start") && rightHandState.get("grab-start").matches(".pen, .pen *");
  const rightHandHoldingCamera =
    !cursorGrabbing &&
    rightHandState.has("grab-start") &&
    rightHandState.get("grab-start").matches(".icamera, .icamera *");

  const rightHandHoveringOnNothing =
    !rightHandTeleporting &&
    !rightHandHovering &&
    !cursorHand.has("hover-start") &&
    !cursorGrabbing &&
    !rightHandState.has("grab-start");

  // Cursor
  cursorController.enabled = !(rightHandTeleporting || rightHandHovering || rightHandGrabbing);
  const cursorHoldingUI =
    cursorController.enabled &&
    !rightHandTeleporting &&
    !rightHandHovering &&
    !rightHandGrabbing &&
    (cursorHand.has("grab-start") && cursorHand.get("grab-start").matches(".ui, .ui *"));
  const cursorHoveringOnUI = cursorHand.has("hover-start") && cursorHand.get("hover-start").matches(".ui, .ui *");
  const cursorHoveringOnActionable =
    cursorHand.has("hover-start") && cursorHand.get("hover-start").matches(".actionable, .actionable *");

  const cursorCanHover =
    cursorController.enabled &&
    !rightHandTeleporting &&
    !rightHandHovering &&
    !rightHandGrabbing &&
    !cursorHoveringOnUI &&
    !cursorHoveringOnActionable &&
    cursorHand.has("hover-start");

  const cursorHoveringOnInteractable =
    cursorCanHover && cursorHand.get("hover-start").matches(".interactable, .interactable *");
  const cursorHoveringOnTextureLightElectableElement =
    cursorHand.get("hover-start") &&
    cursorHand.get("hover-start").matches(".night-mode-actionable, .night-mode-actionable *") &&
    !cursorHand.get("hover-start").matches(".night-mode-actionable-disabled, .night-mode-actionable-disabled *") &&
    nightModeActive;

  const cursorHoveringOnCamera = cursorCanHover && cursorHand.get("hover-start").matches(".icamera, .icamera *");
  const cursorHoveringOnPen = cursorCanHover && cursorHand.get("hover-start").matches(".pen, .pen *");
  const cursorHoveringOnVideo = cursorCanHover && cursorHand.get("hover-start").components["media-video"];
  const cursorHoldingInteractable =
    !rightHandTeleporting &&
    cursorHand.has("grab-start") &&
    cursorHand.get("grab-start").matches(".interactable, .interactable *");
  // const cursorHoldingPen =
  //   !rightHandTeleporting && cursorHand.has("grab-start") && cursorHand.get("grab-start").matches(".pen, .pen *");
  const cursorHoldingPen = !rightHandTeleporting && penActive;
  const cursorHoldingCamera =
    !rightTeleporter.isTeleporting &&
    cursorHand.has("grab-start") &&
    cursorHand.get("grab-start").matches(".icamera, .icamera *");

  const cursorHoveringOnNothing =
    !cursorHoldingPen &&
    !rightHandTeleporting &&
    !rightHandHovering &&
    !rightHandGrabbing &&
    !cursorHand.has("hover-start") &&
    !cursorHand.has("grab-start");

  const cursorShowingTeleportIndicator =
    (cursorTeleport && cursorTeleport.active) || rightHandTeleporting || leftHandTeleporting;

  const isFlatInputFocused =
    document.activeElement.nodeName === "INPUT" || document.activeElement.nodeName === "TEXTAREA" || document.activeElement.contentEditable === "true";
  const isVRInputFocused = vrInputSystem && vrInputSystem.isInputFocused();

  const userinput = AFRAME.scenes[0].systems.userinput;
  userinput.toggleSet(sets.leftHandHoveringOnInteractable, leftHandHoveringOnInteractable);
  userinput.toggleSet(sets.leftHandHoveringOnPen, leftHandHoveringOnPen);
  userinput.toggleSet(sets.leftHandHoveringOnCamera, leftHandHoveringOnCamera);
  userinput.toggleSet(sets.leftHandHoveringOnNothing, leftHandHoveringOnNothing);
  userinput.toggleSet(sets.leftHandHoldingPen, leftHandHoldingPen);
  userinput.toggleSet(sets.leftHandHoldingInteractable, leftHandHoldingInteractable);
  userinput.toggleSet(sets.leftHandHoldingCamera, leftHandHoldingCamera);
  userinput.toggleSet(sets.leftHandTeleporting, leftHandTeleporting);

  userinput.toggleSet(sets.rightHandHoveringOnInteractable, rightHandHoveringOnInteractable);
  userinput.toggleSet(sets.rightHandHoveringOnPen, rightHandHoveringOnPen);
  userinput.toggleSet(sets.rightHandHoveringOnNothing, rightHandHoveringOnNothing);
  userinput.toggleSet(sets.rightHandHoveringOnActionable, rightHandHoveringOnActionable);
  userinput.toggleSet(sets.rightHandHoveringOnCamera, rightHandHoveringOnCamera);
  userinput.toggleSet(sets.rightHandHoldingPen, rightHandHoldingPen);
  userinput.toggleSet(sets.rightHandHoldingInteractable, rightHandHoldingInteractable);
  userinput.toggleSet(sets.rightHandTeleporting, rightHandTeleporting);
  userinput.toggleSet(sets.rightHandHoldingCamera, rightHandHoldingCamera);

  userinput.toggleSet(sets.cursorHoveringOnPen, cursorHoveringOnPen);
  userinput.toggleSet(sets.cursorHoveringOnCamera, cursorHoveringOnCamera);
  userinput.toggleSet(sets.cursorHoveringOnInteractable, cursorHoveringOnInteractable);
  userinput.toggleSet(sets.cursorHoveringOnTextureLightActionableElement, cursorHoveringOnTextureLightElectableElement);
  userinput.toggleSet(
    sets.cursorHoveringOnActionable,
    cursorHoveringOnActionable && !cursorHoveringOnTextureLightElectableElement
  );
  userinput.toggleSet(sets.cursorHoveringOnUI, cursorHoveringOnUI);
  userinput.toggleSet(sets.cursorHoveringOnVideo, cursorHoveringOnVideo);
  userinput.toggleSet(sets.cursorHoveringOnNothing, cursorHoveringOnNothing);
  userinput.toggleSet(sets.cursorHoldingPen, cursorHoldingPen);
  userinput.toggleSet(sets.cursorHoldingCamera, cursorHoldingCamera);
  userinput.toggleSet(sets.cursorHoldingInteractable, cursorHoldingInteractable);
  userinput.toggleSet(sets.cursorHoldingUI, cursorHoldingUI);
  userinput.toggleSet(sets.inputFocused, isFlatInputFocused || isVRInputFocused);
  userinput.toggleSet(sets.cursorShowingTeleportIndicator, cursorShowingTeleportIndicator);
  userinput.toggleSet(sets.npcActive, npcActive);
}
