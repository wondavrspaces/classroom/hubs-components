import { paths } from "../paths";
import { sets } from "../sets";
import { xforms } from "./xforms";
import { addSetsToBindings } from "./utils";

const name = "/webxr/var/";

const leftButton = paths.device.webxr.left.button;
const leftAxis = paths.device.webxr.left.axis;
const leftPose = paths.device.webxr.left.pose;
const rightButton = paths.device.webxr.right.button;
const rightAxis = paths.device.webxr.right.axis;
const rightPose = paths.device.webxr.right.pose;

const leftJoyXDeadzoned = `${name}left/joy/x/deadzoned`;
const leftJoyYDeadzoned = `${name}left/joy/y/deadzoned`;
const scaledLeftJoyX = `${name}left/scaledJoyX`;
const scaledLeftJoyY = `${name}left/scaledJoyY`;
const cursorDrop2 = `${name}right/cursorDrop2`;
const cursorDrop1 = `${name}right/cursorDrop1`;
const rightHandDrop2 = `${name}right/rightHandDrop2`;
const rightGripRisingGrab = `${name}right/grip/RisingGrab`;
const rightTriggerRisingGrab = `${name}right/trigger/RisingGrab`;
const leftGripRisingGrab = `${name}left/grip/RisingGrab`;
const leftTriggerRisingGrab = `${name}left/trigger/RisingGrab`;
const rightDpadNorth = `${name}rightDpad/north`;
const rightDpadSouth = `${name}rightDpad/south`;
const rightDpadEast = `${name}rightDpad/east`;
const rightDpadWest = `${name}rightDpad/west`;
const rightDpadCenter = `${name}rightDpad/center`;
const rightJoy = `${name}right/joy`;
const rightJoyY1 = `${name}right/joyY1`;
const rightJoyYDeadzoned = `${name}right/joy/y/deadzoned`;
const leftDpadNorth = `${name}leftDpad/north`;
const leftDpadSouth = `${name}leftDpad/south`;
const leftDpadEast = `${name}leftDpad/east`;
const leftDpadWest = `${name}leftDpad/west`;
const leftDpadCenter = `${name}leftDpad/center`;
const leftJoy = `${name}left/joy`;
const characterAcceleration = "/var/webxr/nonNormalizedCharacterAcceleration";
const rightBoost = "/var/right-webxr/boost";
const leftBoost = "/var/left-webxr/boost";

const lowerButtons = `${name}buttons/lower`;
const upperButtons = `${name}buttons/upper`;
const touchpad = `${name}touchpad`;
const touchpadRising = `${name}touchpad/rising`;
const dpadNorth = `${name}dpadNorth`;
const dpadSouth = `${name}dpadSouth`;
const dpadEast = `${name}dpadEast`;
const dpadWest = `${name}dpadWest`;
const dpadCenter = `${name}dpadCenter`;
const dpadCenterStrip = `${name}dpadCenterStrip`;
const snapRotateRight1 = `${name}snapRotateRight1`;
const snapRotateRight2 = `${name}snapRotateRight2`;
const snapRotateLeft1 = `${name}snapRotateLeft1`;
const snapRotateLeft2 = `${name}snapRotateLeft2`;
const centerStripPressed = `${name}centerStripPressed`;
const touchpadReleased = `${name}touchpadReleased`;
const lowerButtonsReleased = `${name}lowerButtonsReleased`;

const v = s => {
  return "/webxr-user-vars/" + s;
};
const leftGripPressed1 = v("leftGripPressed1");
const leftGripPressed2 = v("leftGripPressed2");
const rightGripPressed1 = v("rightGripPressed1");
const rightGripPressed2 = v("rightGripPressed2");
const leftTriggerPressed1 = v("leftTriggerPressed1");
const leftTriggerPressed2 = v("leftTriggerPressed2");
const rightTriggerPressed1 = v("rightTriggerPressed1");
const rightTriggerPressed2 = v("rightTriggerPressed2");
const penNextColor1 = v("penNextColor1");
const penNextColor2 = v("penNextColor2");
const penPrevColor1 = v("penPrevColor1");
const penPrevColor2 = v("penPrevColor2");
const penDrop1 = v("penDrop1");
const penDrop2 = v("penDrop2");
const penUndo1 = v("penUndo1");
const penUndo2 = v("penUndo2");
const scalePenTip1 = v("scalePenTip1");
const scalePenTip2 = v("scalePenTip2");
export const webXRUserBindings = addSetsToBindings({
  [sets.global]: [
    {
      src: { value: paths.device.webxr.left.matrix },
      dest: { value: paths.actions.leftHand.matrix },
      xform: xforms.copy
    },
    {
      src: { value: paths.device.webxr.right.matrix },
      dest: { value: paths.actions.rightHand.matrix },
      xform: xforms.copy
    },
    {
      src: { value: leftButton.grip.pressed },
      dest: { value: leftGripPressed1 },
      xform: xforms.copy
    },
    {
      src: { value: leftButton.grip.pressed },
      dest: { value: leftGripPressed2 },
      xform: xforms.copy
    },
    {
      src: { value: leftGripPressed1 },
      dest: { value: paths.actions.leftHand.middleRingPinky },
      xform: xforms.copy
    },
    {
      src: [leftButton.a.touched, leftButton.b.touched, leftButton.thumbStick.touched],
      dest: { value: paths.actions.leftHand.thumb },
      xform: xforms.any
    },
    {
      src: { value: leftButton.trigger.pressed },
      dest: { value: leftTriggerPressed1 },
      xform: xforms.copy
    },
    {
      src: { value: leftButton.trigger.pressed },
      dest: { value: leftTriggerPressed2 },
      xform: xforms.copy
    },
    {
      src: { value: leftTriggerPressed1 },
      dest: { value: paths.actions.leftHand.index },
      xform: xforms.copy
    },
    {
      src: { value: rightButton.grip.pressed },
      dest: { value: rightGripPressed1 },
      xform: xforms.copy
    },
    {
      src: { value: rightButton.grip.pressed },
      dest: { value: rightGripPressed2 },
      xform: xforms.copy
    },
    {
      src: { value: rightGripPressed1 },
      dest: { value: paths.actions.rightHand.middleRingPinky },
      xform: xforms.copy
    },
    {
      src: [rightButton.a.touched, rightButton.b.touched, rightButton.thumbStick.touched],
      dest: { value: paths.actions.rightHand.thumb },
      xform: xforms.any
    },
    {
      src: { value: rightButton.trigger.pressed },
      dest: { value: rightTriggerPressed1 },
      xform: xforms.copy
    },
    {
      src: { value: rightButton.trigger.pressed },
      dest: { value: rightTriggerPressed2 },
      xform: xforms.copy
    },
    {
      src: { value: rightTriggerPressed1 },
      dest: { value: paths.actions.rightHand.index },
      xform: xforms.copy
    },
    {
      src: {
        x: leftAxis.joyX,
        y: leftAxis.joyY
      },
      dest: { value: leftJoy },
      xform: xforms.compose_vec2
    },
    {
      src: { value: leftJoy },
      dest: {
        north: leftDpadNorth,
        south: leftDpadSouth,
        east: leftDpadEast,
        west: leftDpadWest,
        center: leftDpadCenter
      },
      xform: xforms.vec2dpad(0.2, false, true)
    },
    {
      src: { value: rightAxis.joyY },
      dest: { value: rightJoyY1 },
      xform: xforms.copy
    },
    {
      src: {
        x: rightAxis.joyX,
        y: rightJoyY1
      },
      dest: { value: rightJoy },
      xform: xforms.compose_vec2
    },
    {
      src: { value: rightJoy },
      dest: {
        north: rightDpadNorth,
        south: rightDpadSouth,
        east: rightDpadEast,
        west: rightDpadWest,
        center: rightDpadCenter
      },
      xform: xforms.vec2dpad(0.2, false, true)
    },
    {
      src: { value: rightDpadEast },
      dest: { value: paths.actions.snapRotateRight },
      xform: xforms.rising,
      priority: 1
    },
    {
      src: [leftButton.a.pressed, rightButton.a.pressed],
      dest: { value: lowerButtons },
      xform: xforms.any
    },
    {
      src: [leftButton.b.pressed, rightButton.b.pressed],
      dest: { value: upperButtons },
      xform: xforms.any
    },
    {
      src: { value: rightDpadWest },
      dest: { value: paths.actions.snapRotateLeft },
      xform: xforms.rising,
      priority: 1
    },
    {
      src: { value: leftAxis.joyY },
      dest: { value: leftJoyYDeadzoned },
      xform: xforms.deadzone(0.1)
    },
    {
      src: { value: leftJoyYDeadzoned },
      dest: { value: scaledLeftJoyY },
      xform: xforms.scale(-0.7) // horizontal character speed modifier
    },
    {
      src: { value: leftAxis.joyX },
      dest: { value: leftJoyXDeadzoned },
      xform: xforms.deadzone(0.1)
    },
    {
      src: { value: leftJoyXDeadzoned },
      dest: { value: scaledLeftJoyX },
      xform: xforms.scale(0.7) // horizontal character speed modifier
    },
    {
      src: {
        x: scaledLeftJoyX,
        y: scaledLeftJoyY
      },
      dest: { value: characterAcceleration },
      xform: xforms.compose_vec2
    },
    {
      src: { value: characterAcceleration },
      dest: { value: paths.actions.characterAcceleration },
      xform: xforms.copy
    },
    {
      src: { value: leftButton.b.pressed },
      dest: { value: leftBoost },
      xform: xforms.copy
    },
    {
      src: { value: rightButton.b.pressed },
      dest: { value: rightBoost },
      xform: xforms.copy
    },
    {
      src: [leftBoost, rightBoost],
      dest: { value: paths.actions.boost },
      xform: xforms.any
    },
    {
      src: { value: rightPose },
      dest: { value: paths.actions.cursor.pose },
      xform: xforms.copy
    },
    {
      src: { value: rightPose },
      dest: { value: paths.actions.rightHand.pose },
      xform: xforms.copy
    },
    {
      src: { value: leftPose },
      dest: { value: paths.actions.leftHand.pose },
      xform: xforms.copy
    },
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.rightHand.stopTeleport },
      xform: xforms.falling,
      priority: 1
    },
    {
      src: { value: leftTriggerPressed2 },
      dest: { value: paths.actions.leftHand.stopTeleport },
      xform: xforms.falling,
      priority: 1
    },
    {
      src: {
        x: rightAxis.touchpadX,
        y: rightAxis.touchpadY
      },
      dest: { value: touchpad },
      xform: xforms.compose_vec2
    },
    {
      src: {
        value: touchpad
      },
      dest: {
        north: dpadNorth,
        south: dpadSouth,
        east: dpadEast,
        west: dpadWest,
        center: dpadCenter
      },
      xform: xforms.vec2dpad(0.3)
    },
    {
      src: [dpadNorth, dpadCenter, dpadSouth],
      dest: { value: dpadCenterStrip },
      xform: xforms.any
    },
    {
      src: {
        value: dpadCenterStrip,
        bool: rightButton.touchpad.pressed
      },
      dest: { value: centerStripPressed },
      xform: xforms.copyIfTrue
    },
    {
      src: [centerStripPressed, lowerButtons],
      dest: { value: paths.actions.ensureFrozen },
      xform: xforms.any
    },
    {
      src: { value: rightButton.touchpad.pressed },
      dest: { value: touchpadReleased },
      xform: xforms.falling
    },
    {
      src: { value: lowerButtons },
      dest: { value: lowerButtonsReleased },
      xform: xforms.falling
    },
    {
      src: [touchpadReleased, lowerButtonsReleased],
      dest: { value: paths.actions.thaw },
      xform: xforms.any
    },
    {
      src: { value: rightButton.touchpad.pressed },
      dest: { value: touchpadRising },
      xform: xforms.rising
    },
    {
      src: {
        value: dpadEast,
        bool: touchpadRising
      },
      dest: { value: snapRotateRight1 },
      xform: xforms.copyIfTrue
    },
    {
      src: {
        value: dpadWest,
        bool: touchpadRising
      },
      dest: { value: snapRotateLeft1 },
      xform: xforms.copyIfTrue
    },
    {
      src: { value: rightDpadEast },
      dest: { value: snapRotateRight2 },
      xform: xforms.rising,
      priority: 1
    },
    {
      src: { value: rightDpadWest },
      dest: { value: snapRotateLeft2 },
      xform: xforms.rising,
      priority: 1
    },
    {
      src: [snapRotateRight1, snapRotateRight2],
      dest: { value: paths.actions.snapRotateRight },
      xform: xforms.any
    },
    {
      src: [snapRotateLeft1, snapRotateLeft2],
      dest: { value: paths.actions.snapRotateLeft },
      xform: xforms.any
    }
  ],

  [sets.leftHandHoveringOnNothing]: [
    {
      src: { value: leftTriggerPressed2 },
      dest: { value: paths.actions.leftHand.startTeleport },
      xform: xforms.downForDelay()
    },
    {
      src: { value: leftTriggerPressed2 },
      dest: { value: paths.actions.leftHand.clickNowhere },
      xform: xforms.downThenReleasedInDelay()
    }
  ],

  [sets.cursorHoveringOnUI]: [
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.cursor.grab },
      xform: xforms.rising,
      priority: 2
    }
  ],

  [sets.cursorHoveringOnNothing]: [
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.rightHand.startTeleport },
      xform: xforms.downForDelay(),
      priority: 1
    },
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.rightHand.clickNowhere },
      xform: xforms.downThenReleasedInDelay(),
      priority: 1
    }
  ],

  [sets.leftHandHoveringOnInteractable]: [
    {
      src: { value: leftGripPressed2 },
      dest: { value: leftGripRisingGrab },
      xform: xforms.rising
    },
    {
      src: { value: leftTriggerPressed2 },
      dest: { value: leftTriggerRisingGrab },
      xform: xforms.rising,
      priority: 2
    },
    {
      src: [leftGripRisingGrab],
      dest: { value: paths.actions.leftHand.grab },
      xform: xforms.any,
      priority: 2
    }
  ],

  [sets.leftHandHoldingInteractable]: [
    {
      src: { value: leftGripPressed2 },
      dest: { value: paths.actions.leftHand.drop },
      xform: xforms.falling
    }
  ],

  [sets.cursorHoveringOnInteractable]: [
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.cursor.grab },
      xform: xforms.rising,
      priority: 2
    }
  ],

  [sets.cursorHoldingUI]: [
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: cursorDrop2 },
      xform: xforms.falling,
      priority: 5
    },
    {
      src: [cursorDrop2],
      dest: { value: paths.actions.cursor.drop },
      xform: xforms.any,
      priority: 5
    }
  ],

  [sets.cursorHoveringOnActionable]: [
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.cursor.clickActionable },
      xform: xforms.downThenReleasedInDelay(),
      priority: 3
    }
  ],

  [sets.cursorHoveringOnTextureLightActionableElement]: [
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.cursor.clickNightModeActionable },
      xform: xforms.downThenReleasedInDelay(),
      priority: 3
    }
  ],

  [sets.cursorHoldingInteractable]: [
    {
      src: { value: rightAxis.joyY },
      dest: { value: paths.actions.cursor.modDelta },
      xform: xforms.scale(0.1)
    },
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: cursorDrop2 },
      xform: xforms.falling,
      priority: 5
    },
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.cursor.clickInteractive },
      xform: xforms.releasedInDelay(200),
      priority: 5
    },
    {
      src: [cursorDrop2],
      dest: { value: paths.actions.cursor.drop },
      xform: xforms.any,
      priority: 2
    }
  ],

  [sets.cursorHoldingPen]: [
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.cursor.startDrawing },
      xform: xforms.rising,
      priority: 3
    },
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: paths.actions.cursor.stopDrawing },
      xform: xforms.falling,
      priority: 3
    },
    {
      src: { value: rightButton.b.pressed },
      dest: { value: penUndo1 },
      xform: xforms.rising,
      priority: 1
    },
    {
      src: { value: rightButton.a.pressed },
      dest: { value: paths.actions.cursor.switchDrawMode },
      xform: xforms.rising,
      priority: 1
    },
    {
      src: {
        value: rightDpadEast
      },
      dest: {
        value: penNextColor1
      },
      xform: xforms.rising,
      priority: 5
    },
    {
      src: {
        value: rightDpadWest
      },
      dest: {
        value: penPrevColor1
      },
      xform: xforms.rising,
      priority: 5
    },
    {
      src: {
        value: rightAxis.joyY
      },
      dest: {
        value: rightJoyYDeadzoned
      },
      xform: xforms.deadzone(0.1)
    },
    {
      src: { value: rightJoyYDeadzoned },
      dest: { value: scalePenTip1 },
      xform: xforms.scaleExp(-0.005, 5),
      priority: 2
    },
    {
      src: { value: rightGripPressed2 },
      dest: { value: penDrop1 },
      xform: xforms.falling,
      priority: 3
    },
    {
      src: {
        value: dpadEast,
        bool: touchpadRising
      },
      dest: { value: penNextColor2 },
      xform: xforms.copyIfTrue,
      priority: 5
    },
    {
      src: {
        value: dpadWest,
        bool: touchpadRising
      },
      dest: { value: penPrevColor2 },
      xform: xforms.copyIfTrue,
      priority: 5
    },
    {
      src: {
        value: dpadSouth,
        bool: touchpadRising
      },
      dest: { value: penDrop2 },
      xform: xforms.copyIfTrue,
      priority: 5
    },
    {
      src: {
        value: dpadNorth,
        bool: touchpadRising
      },
      dest: { value: penUndo2 },
      xform: xforms.copyIfTrue,
      priority: 5
    },
    {
      src: {
        value: rightAxis.touchpadY,
        touching: rightButton.touchpad.touched
      },
      dest: { value: scalePenTip2 },
      xform: xforms.touch_axis_scroll(-0.1)
    },
    {
      src: [penNextColor1, penNextColor2],
      dest: { value: paths.actions.cursor.penNextColor },
      xform: xforms.any
    },
    {
      src: [penPrevColor1, penPrevColor2],
      dest: { value: paths.actions.cursor.penPrevColor },
      xform: xforms.any
    },
    {
      src: [penDrop1, penDrop2],
      dest: { value: paths.actions.cursor.drop },
      xform: xforms.any
    },
    {
      src: [penUndo1, penUndo2],
      dest: { value: paths.actions.cursor.undoDrawing },
      xform: xforms.any
    },
    {
      src: [scalePenTip1, scalePenTip2],
      dest: { value: paths.actions.cursor.scalePenTip },
      xform: xforms.anyValue
    }
  ],
  [sets.npcActive]: [
    {
      src: { value: rightButton.a.pressed },
      dest: { value: paths.actions.rightHand.startSpeaking},
      xform: xforms.rising,
      priority: 3
    },
    {
      src: { value: rightButton.a.pressed },
      dest: { value: paths.actions.rightHand.stopSpeaking },
      xform: xforms.releasedInDelay(60000),
      priority: 3
    },
    {
      src: { value: rightButton.b.pressed },
      dest: { value: paths.actions.rightHand.resetConversation},
      xform: xforms.rising,
      priority: 3
    }
  ],
  [sets.rightHandHoveringOnInteractable]: [
    {
      src: { value: rightGripPressed2 },
      dest: { value: rightGripRisingGrab },
      xform: xforms.rising,
      priority: 2
    },
    {
      src: { value: rightTriggerPressed2 },
      dest: { value: rightTriggerRisingGrab },
      xform: xforms.rising,
      priority: 2
    },
    {
      src: [rightGripRisingGrab],
      dest: { value: paths.actions.rightHand.grab },
      xform: xforms.any,
      priority: 2
    }
  ],

  [sets.rightHandHoldingInteractable]: [
    {
      src: { value: rightGripPressed2 },
      dest: {
        value: rightHandDrop2
      },
      xform: xforms.falling,
      priority: 3
    },
    {
      src: [rightHandDrop2],
      dest: { value: paths.actions.rightHand.drop },
      xform: xforms.any,
      priority: 2
    }
  ],
  [sets.rightHandHoveringOnPen]: [],

  [sets.rightHandHoveringOnNothing]: [],
  [sets.inputFocused]: []
});
