import { paths } from "../paths";
import { sets } from "../sets";
import { xforms } from "./xforms";
import { addSetsToBindings } from "./utils";

const wasd_vec2 = "/var/mouse-and-keyboard/wasd_vec2";
const keyboardCharacterAcceleration = "/var/mouse-and-keyboard/keyboardCharacterAcceleration";
const arrows_vec2 = "/var/mouse-and-keyboard/arrows_vec2";
const dropWithRMB = "/vars/mouse-and-keyboard/drop_with_RMB";
const dropWithEsc = "/vars/mouse-and-keyboard/drop_with_esc";

const k = name => {
  return `/keyboard-mouse-user/keyboard-var/${name}`;
};

export const keyboardMouseUserBindings = addSetsToBindings({
  [sets.global]: [
    {
      src: {
        w: paths.device.keyboard.key("arrowup"),
        a: paths.device.keyboard.key("arrowleft"),
        s: paths.device.keyboard.key("arrowdown"),
        d: paths.device.keyboard.key("arrowright")
      },
      dest: { vec2: arrows_vec2 },
      xform: xforms.wasd_to_vec2
    },
    {
      src: {
        w: paths.device.keyboard.code("KeyW"),
        a: paths.device.keyboard.code("KeyA"),
        s: paths.device.keyboard.code("KeyS"),
        d: paths.device.keyboard.code("KeyD"),
        bool1: paths.device.keyboard.key("control"),
        bool2: paths.device.keyboard.key("meta")
      },
      dest: { vec2: wasd_vec2 },
      xform: xforms.wasd_to_vec2IfFalse
    },
    {
      src: {
        first: wasd_vec2,
        second: arrows_vec2
      },
      dest: { value: keyboardCharacterAcceleration },
      xform: xforms.max_vec2
    },
    {
      src: { value: keyboardCharacterAcceleration },
      dest: { value: paths.actions.characterAcceleration },
      xform: xforms.normalize_vec2
    },
    {
      src: { value: paths.device.keyboard.key("shift") },
      dest: { value: paths.actions.boost },
      xform: xforms.copy
    },
    {
      src: { value: paths.device.keyboard.key("Escape") },
      dest: { value: paths.actions.camera.exitMirror },
      xform: xforms.falling
    },
    {
      src: {
        bool: paths.device.keyboard.key("control"),
        value: paths.device.keyboard.key("z")
      },
      dest: { value: paths.actions.shortcut.ctrlZ },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("meta"),
        value: paths.device.keyboard.key("z")
      },
      dest: { value: paths.actions.shortcut.metaZ },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("meta"),
        value: paths.device.keyboard.key("y")
      },
      dest: { value: paths.actions.shortcut.metaY },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("control"),
        value: paths.device.keyboard.key("y")
      },
      dest: { value: paths.actions.shortcut.ctrlY },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("meta"),
        value: paths.device.keyboard.key("c")
      },
      dest: { value: paths.actions.shortcut.metaC },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("control"),
        value: paths.device.keyboard.key("c")
      },
      dest: { value: paths.actions.shortcut.ctrlC },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("meta"),
        value: paths.device.keyboard.key("v")
      },
      dest: { value: paths.actions.shortcut.metaV },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("control"),
        value: paths.device.keyboard.key("v")
      },
      dest: { value: paths.actions.shortcut.ctrlV },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        value: paths.device.keyboard.code("Backspace")
      },
      dest: { value: paths.actions.shortcut.backspace },
      xform: xforms.rising
    },
    {
      src: {
        value: paths.device.keyboard.code("Delete")
      },
      dest: { value: paths.actions.shortcut.delete },
      xform: xforms.rising
    },
    {
      src: {
        bool: paths.device.keyboard.key("shift"),
        value: paths.device.keyboard.key("r")
      },
      dest: { value: paths.actions.shortcut.shiftR },
      xform: xforms.risingIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("shift"),
        value: paths.device.keyboard.key("p")
      },
      dest: { value: paths.actions.shortcut.shiftP },
      xform: xforms.risingIfTrue
    },
    {
      src: { value: paths.device.hud.penButton },
      dest: { value: paths.actions.spawnPen },
      xform: xforms.rising
    },
    {
      src: { value: paths.device.smartMouse.cursorPose },
      dest: { value: paths.actions.cursor.pose },
      xform: xforms.copy
    },
    {
      src: { value: paths.device.smartMouse.cameraDelta },
      dest: { x: "/var/smartMouseCamDeltaX", y: "/var/smartMouseCamDeltaY" },
      xform: xforms.split_vec2
    },
    {
      src: { value: "/var/smartMouseCamDeltaX" },
      dest: { value: "/var/smartMouseCamDeltaXScaled" },
      xform: xforms.scale(-0.2)
    },
    {
      src: { value: "/var/smartMouseCamDeltaY" },
      dest: { value: "/var/smartMouseCamDeltaYScaled" },
      xform: xforms.scale(-0.1)
    },
    {
      src: { x: "/var/smartMouseCamDeltaXScaled", y: "/var/smartMouseCamDeltaYScaled" },
      dest: { value: paths.actions.cameraDelta },
      xform: xforms.compose_vec2
    }
  ],

  [sets.cursorHoldingPen]: [
    {
      src: {
        value: paths.device.keyboard.key("c")
      },
      dest: { value: paths.actions.cursor.penNextColor },
      xform: xforms.rising
    },
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.startDrawing },
      xform: xforms.rising,
      priority: 3
    },
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.stopDrawing },
      xform: xforms.falling,
      priority: 3
    },
    {
      src: {
        bool: paths.device.keyboard.key("shift"),
        value: paths.device.mouse.wheel
      },
      dest: {
        value: k("wheelWithShift")
      },
      xform: xforms.copyIfTrue
    },
    {
      src: {
        value: k("wheelWithShift")
      },
      dest: { value: "/var/cursorScalePenTipWheel" },
      xform: xforms.copy,
      priority: 200
    },
    {
      src: { value: "/var/cursorScalePenTipWheel" },
      dest: { value: paths.actions.cursor.scalePenTip },
      xform: xforms.scale(0.03)
    },
    {
      src: { value: paths.device.mouse.buttonRight },
      dest: { value: dropWithRMB },
      xform: xforms.falling,
      priority: 200
    },
    {
      src: { value: paths.device.keyboard.key("Escape") },
      dest: { value: dropWithEsc },
      xform: xforms.falling
    },
    {
      src: [dropWithRMB, dropWithEsc],
      dest: { value: paths.actions.cursor.drop },
      xform: xforms.any
    }
  ],

  [sets.npcActive]: [
    {
      src: {
        value: paths.device.keyboard.key("a")
      },
      dest: { value: paths.actions.rightHand.startSpeaking },
      xform: xforms.rising
    },
    {
      src: {
        value: paths.device.keyboard.key("a")
      },
      dest: { value: paths.actions.rightHand.stopSpeaking },
      xform: xforms.releasedInDelay(60000)
    },
    {
      src: {
        value: paths.device.keyboard.key("b")
      },
      dest: { value: paths.actions.rightHand.resetConversation },
      xform: xforms.rising
    }
  ],

  [sets.cursorHoldingCamera]: [
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.drop },
      xform: xforms.falling,
      priority: 2
    }
  ],

  [sets.cursorHoldingInteractable]: [
    {
      src: {
        bool: paths.device.keyboard.key("shift"),
        value: paths.device.mouse.wheel
      },
      dest: {
        value: k("wheelWithShift")
      },
      xform: xforms.copyIfTrue
    },
    {
      src: {
        bool: paths.device.keyboard.key("shift"),
        value: paths.device.mouse.wheel
      },
      dest: {
        value: k("wheelWithoutShift")
      },
      xform: xforms.copyIfFalse
    },
    {
      src: {
        value: k("wheelWithoutShift")
      },
      dest: { value: paths.actions.cursor.modDelta },
      xform: xforms.copy
    },
    {
      src: {
        value: k("wheelWithShift")
      },
      dest: { value: paths.actions.cursor.scaleGrabbedGrabbable },
      xform: xforms.copy
    },
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.clickInteractive },
      xform: xforms.releasedInDelay(150),
      priority: 2
    },
    {
      src: { value: paths.device.mouse.buttonLeft || paths.device.keyboard("control") && paths.device.keyboard.key("z") },
      dest: { value: paths.actions.cursor.drop },
      xform: xforms.falling,
      priority: 2
    }
  ],
  [sets.cursorHoldingUI]: [
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.drop },
      xform: xforms.falling,
      priority: 3
    },
    {
      src: { value: paths.device.keyboard.key("shift") },
      dest: { value: paths.actions.rotateModifier },
      xform: xforms.copy
    }
  ],
  [sets.cursorHoveringOnInteractable]: [
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.grab },
      xform: xforms.rising,
      priority: 1
    }
  ],
  [sets.cursorHoveringOnVideo]: [
    {
      src: { value: paths.device.mouse.wheel },
      dest: { value: paths.actions.cursor.mediaVolumeMod },
      xform: xforms.scale(-0.3)
    }
  ],
  [sets.inputFocused]: [
    {
      src: { value: "/device/keyboard" },
      dest: { value: paths.noop },
      xform: xforms.noop,
      priority: 1000
    }
  ],

  [sets.cursorHoveringOnUI]: [
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.grab },
      xform: xforms.rising,
      priority: 1000
    }
  ],

  [sets.cursorHoveringOnActionable]: [
    {
      src: { value: paths.device.mouse.buttonLeft, mouseCoords: paths.device.mouse.coords },
      dest: { value: paths.actions.cursor.clickActionable },
      xform: xforms.downThenReleasedInDelayAndMouseIdle(750),
      priority: 1001
    }
  ],

  [sets.cursorHoveringOnTextureLightActionableElement]: [
    {
      src: { value: paths.device.mouse.buttonLeft, mouseCoords: paths.device.mouse.coords },
      dest: { value: paths.actions.cursor.clickNightModeActionable },
      xform: xforms.downThenReleasedInDelayAndMouseIdle(750),
      priority: 1000
    }
  ],

  // Wonda --> emit event when click on nothing
  [sets.cursorHoveringOnNothing]: [
    {
      src: { value: paths.device.mouse.buttonLeft },
      dest: { value: paths.actions.cursor.clickNowhere },
      xform: xforms.downThenReleasedInDelay(300),
      priority: 99
    }
  ]
});
