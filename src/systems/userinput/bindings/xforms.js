import { Pose } from "../pose";
import { angleTo4Direction } from "../../../utils/dpad";

export const xforms = {
  noop: function() {},
  copy: function(frame, src, dest) {
    frame.setValueType(dest.value, frame.get(src.value));
  },
  scale: function(scalar) {
    return function scale(frame, src, dest) {
      if (frame.get(src.value) !== undefined) {
        frame.setValueType(dest.value, frame.get(src.value) * scalar);
      }
    };
  },
  scaleExp: function(scalar, exp = 1) {
    return function scale(frame, src, dest) {
      if (frame.get(src.value) !== undefined) {
        if (exp === 1) {
          frame.setValueType(dest.value, frame.get(src.value) * scalar);
        } else {
          frame.setValueType(dest.value, Math.pow(frame.get(src.value), exp) * scalar);
        }
      }
    };
  },
  deadzone: function(deadzoneSize) {
    return function deadzone(frame, src, dest) {
      frame.setValueType(dest.value, Math.abs(frame.get(src.value)) < deadzoneSize ? 0 : frame.get(src.value));
    };
  },
  split_vec2: function(frame, src, dest) {
    const value = frame.get(src.value);
    if (value !== undefined) {
      frame.setValueType(dest.x, value[0]);
      frame.setValueType(dest.y, value[1]);
    }
  },
  compose_vec2: function(frame, src, dest) {
    const x = frame.get(src.x);
    const y = frame.get(src.y);
    if (x !== undefined && y !== undefined) {
      frame.setVector2(dest.value, x, y);
    }
  },
  negate: function(frame, src, dest) {
    frame.setValueType(dest.value, -frame.get(src.value));
  },
  copyIfFalse: function(frame, src, dest) {
    frame.setValueType(dest.value, frame.get(src.bool) ? undefined : frame.get(src.value));
  },
  copyIfTrue: function(frame, src, dest) {
    frame.setValueType(dest.value, frame.get(src.bool) ? frame.get(src.value) : undefined);
  },
  zeroIfDefined: function(frame, src, dest) {
    frame.setValueType(dest.value, frame.get(src.bool) !== undefined ? 0 : frame.get(src.value));
  },
  true: function(frame, src, dest) {
    frame.setValueType(dest.value, true);
  },
  rising: function rising(frame, src, dest, prevState) {
    frame.setValueType(dest.value, frame.get(src.value) && prevState === false);
    return !!frame.get(src.value);
  },
  risingIfTrue: function(frame, src, dest, prevState) {
    const cValue = frame.get(src.bool) ? frame.get(src.value) : undefined;
    frame.setValueType(dest.value, cValue && prevState === false);
    return !!frame.get(src.value);
  },
  risingWithFrameDelay: function(n) {
    return function risingWithFrameDelay(frame, src, dest, state = { values: new Array(n) }) {
      frame.setValueType(dest.value, state.values.shift());
      state.values.push(frame.get(src.value) && !state.prev);
      state.prev = frame.get(src.value);
      return state;
    };
  },
  releasedInDelay: function(time = 750) {
    return function releasedInDelay(frame, src, dest, state = { initTime: null, prev: undefined }) {
      const cTime = Date.now();
      if (!state.initTime) {
        state.initTime = cTime;
        frame.setValueType(dest.value, false);
        return state;
      }

      const isFalling = !frame.get(src.value) && state.prev;
      const isReleasedInDelay = cTime - state.initTime <= time && isFalling;
      if (isFalling) state.initTime = Infinity;

      state.prev = frame.get(src.value);
      frame.setValueType(dest.value, isReleasedInDelay);
      return state;
    };
  },
  downForDelay: function(time = 750) {
    return function downForDelay(frame, src, dest, state = { risingTimestamp: Infinity, prev: undefined }) {
      const cTime = Date.now();
      const isDownForDelay = cTime - state.risingTimestamp >= time;

      const isRising = frame.get(src.value) && state.prev === false;
      if (isRising) state.risingTimestamp = cTime;

      const isFalling = !frame.get(src.value) && state.prev;
      if (isFalling) state.risingTimestamp = Infinity;

      state.prev = frame.get(src.value);
      frame.setValueType(dest.value, isDownForDelay);
      return state;
    };
  },
  downThenReleasedInDelay: function(time = 750) {
    return function downThenReleasedInDelay(
      frame,
      src,
      dest,
      state = { risingTimestamp: Infinity, fallingTimestamp: Infinity, prev: undefined }
    ) {
      const cTime = Date.now();
      const isRising = frame.get(src.value) && state.prev === false;
      const isFalling = !frame.get(src.value) && state.prev;

      if (isFalling) state.fallingTimestamp = cTime;
      if (isRising) state.risingTimestamp = cTime;

      const isClickedInDelay =
        state.fallingTimestamp - state.risingTimestamp <= time && state.fallingTimestamp - state.risingTimestamp >= 0;
      if (isClickedInDelay) {
        state.fallingTimestamp = Infinity;
        state.risingTimestamp = Infinity;
      }
      state.prev = frame.get(src.value);
      frame.setValueType(dest.value, isClickedInDelay);
      return state;
    };
  },
  downThenReleasedInDelayAndMouseIdle: function(time = 750) {
    return function downThenReleasedInDelayAndMouseIdle(
      frame,
      src,
      dest,
      state = {
        risingTimestamp: Infinity,
        fallingTimestamp: Infinity,
        prev: undefined
      }
    ) {
      const cTime = Date.now();
      const mouseMovementTreshold = 0.001;
      const isRising = frame.get(src.value) && state.prev === false;
      const isFalling = !frame.get(src.value) && state.prev;

      if (isFalling) {
        state.fallingTimestamp = cTime;
        state.endMousePos = [...frame.get(src.mouseCoords)];
      }
      if (isRising) {
        state.risingTimestamp = cTime;
        state.initMousePos = [...frame.get(src.mouseCoords)];
      }

      const isClickedInDelay =
        state.fallingTimestamp - state.risingTimestamp <= time && state.fallingTimestamp - state.risingTimestamp >= 0;
      let isCursorIdle = false;
      if (isClickedInDelay) {
        state.fallingTimestamp = Infinity;
        state.risingTimestamp = Infinity;
        isCursorIdle =
          Math.pow(state.endMousePos[0] - state.initMousePos[0], 2) +
            Math.pow(state.endMousePos[1] - state.initMousePos[1], 2) <
          mouseMovementTreshold;
      }
      state.prev = frame.get(src.value);
      frame.setValueType(dest.value, isClickedInDelay && isCursorIdle);
      return state;
    };
  },
  falling: function falling(frame, src, dest, prevState) {
    frame.setValueType(dest.value, !frame.get(src.value) && prevState);
    return !!frame.get(src.value);
  },
  vec2Zero: function(frame, _, dest) {
    frame.setVector2(dest.value, 0, 0);
  },
  poseFromCameraProjection: function() {
    let camera;
    const pose = new Pose();
    return function poseFromCameraProjection(frame, src, dest) {
      if (!camera) {
        camera = document.querySelector("#player-camera").components.camera.camera;
      }
      const value = frame.get(src.value);
      frame.setPose(dest.value, pose.fromCameraProjection(camera, value[0], value[1]));
    };
  },
  vec2dpad: function(deadzoneRadius, invertX = false, invertY = false) {
    const deadzoneRadiusSquared = deadzoneRadius * deadzoneRadius;

    return function vec2dpad(frame, src, dest) {
      if (!frame.get(src.value)) return;
      const [x, y] = frame.get(src.value);
      const inCenter = x * x + y * y < deadzoneRadiusSquared;
      const direction = inCenter ? "center" : angleTo4Direction(Math.atan2(invertX ? -x : x, invertY ? -y : y));
      if (!dest[direction]) return;
      frame.setValueType(dest[direction], true);
    };
  },
  always: function(constValue) {
    return function always(frame, _, dest) {
      frame.setValueType(dest.value, constValue);
    };
  },
  wasd_to_vec2: function(frame, { w, a, s, d }, { vec2 }) {
    let x = 0;
    let y = 0;
    if (frame.get(a)) x -= 1;
    if (frame.get(d)) x += 1;
    if (frame.get(w)) y += 1;
    if (frame.get(s)) y -= 1;
    frame.setVector2(vec2, x, y);
  },
  wasd_to_vec2IfFalse: function(frame, { w, a, s, d, bool1, bool2 }, { vec2 }) {
    const isTrue = frame.get(bool1) || frame.get(bool2);
    if (isTrue) {
      frame.setVector2(vec2, 0, 0);
    } else {
      xforms.wasd_to_vec2(frame, { w, a, s, d}, { vec2});
    }
  },
  add_vec2: function(frame, src, dest) {
    const first = frame.get(src.first);
    const second = frame.get(src.second);
    if (first && second) {
      frame.setVector2(dest.value, first[0] + second[0], first[1] + second[1]);
    } else if (second) {
      frame.setVector2(dest.value, second[0], second[1]);
    } else if (first) {
      frame.setVector2(dest.value, first[0], first[1]);
    }
  },
  max_vec2: function(frame, src, dest) {
    const first = frame.get(src.first);
    const second = frame.get(src.second);
    if (first && second) {
      const max =
        first[0] * first[0] + first[1] * first[1] > second[0] * second[0] + second[1] * second[1] ? first : second;
      frame.setVector2(dest.value, max[0], max[1]);
    } else if (second) {
      frame.setVector2(dest.value, second[0], second[1]);
    } else if (first) {
      frame.setVector2(dest.value, first[0], first[1]);
    }
  },
  normalize_vec2: function(frame, src, dest) {
    const vec2 = frame.get(src.value);
    if (vec2) {
      if (vec2[0] === 0 && vec2[0] === 0) {
        frame.setVector2(dest.value, vec2[0], vec2[1]);
      } else {
        const l = Math.sqrt(vec2[0] * vec2[0] + vec2[1] * vec2[1]);
        frame.setVector2(dest.value, vec2[0] / l, vec2[1] / l);
      }
    }
  },
  all: function(frame, src, dest) {
    for (const path in src) {
      if (!frame.get(src[path])) {
        frame.setValueType(dest.value, false);
        return;
      }
    }
    frame.setValueType(dest.value, true);
  },
  any: function(frame, src, dest) {
    for (const path in src) {
      if (frame.get(src[path])) {
        frame.setValueType(dest.value, true);
        return;
      }
    }
    frame.setValueType(dest.value, false);
  },
  touch_axis_scroll(scale = 1) {
    return function touch_axis_scroll(frame, src, dest, state = { value: 0, touching: false }) {
      frame.setValueType(
        dest.value,
        state.touching && frame.get(src.touching) ? scale * (frame.get(src.value) - state.value) : 0
      );
      state.value = frame.get(src.value);
      state.touching = frame.get(src.touching);
      return state;
    };
  },
  anyValue(frame, src, dest) {
    for (const path in src) {
      if (frame.get(src[path])) {
        frame.setValueType(dest.value, frame.get(src[path]));
        return;
      }
    }
    frame.setValueType(dest.value, 0);
  }
};
