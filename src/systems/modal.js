AFRAME.registerSystem("modal", {
  init: function() {
    this.isModalActive = false;
    this.cursorController = document.querySelector("#cursor-controller");
    this.onModalCloseCb = null;

    this._storedCursorControllerObjects = [];
    this._storedCursorControllerTargetObject = "";
    this._storedCanTeleportState = null;
  },

  activate: function(targetSelector, objectsSelector, onModalCloseCb) {
    if (this.isModalActive) {
      this.deactivate();
    }

    this.onModalCloseCb = onModalCloseCb;

    this.isModalActive = true;
    this.el.emit("modal-open");

    if (!this.cursorController || !this.cursorController.components["cursor-controller"]) return;

    this._storedCursorControllerObjects = this.cursorController.components["cursor-controller"].data.objects;
    this._storedCursorControllerTargetObject = this.cursorController.components["cursor-controller"].data.targetObject;
    this.cursorController.setAttribute("cursor-controller", {
      objects: objectsSelector,
      targetObject: targetSelector
    });

    if (this.cursorController.components["cursor-teleport"]) {
      this._storedCanTeleportState = this.cursorController.components["cursor-teleport"].data.canTeleport;
      this.cursorController.setAttribute("cursor-teleport", { canTeleport: false });
    }
  },

  deactivate: function() {
    if (!this.isModalActive) return;
    this.isModalActive = false;

    if (!this.cursorController || !this.cursorController.components["cursor-controller"]) return;

    this.cursorController.setAttribute("cursor-controller", {
      objects: this._storedCursorControllerObjects,
      targetObject: this._storedCursorControllerTargetObject
    });

    if (this.cursorController.components["cursor-teleport"]) {
      this.cursorController.setAttribute("cursor-teleport", { canTeleport: this._storedCanTeleportState });
      this._storedCanTeleportState = false;
    }

    if (this.onModalCloseCb) {
      this.onModalCloseCb();
      this.onModalCloseCb = null;
    }

    this._storedCursorControllerObjects = [];
    this._storedCursorControllerTargetObject = "";
  }
});
