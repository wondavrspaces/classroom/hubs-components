import { getCurrentSettings } from "..";
import { Pathfinding } from "three-pathfinding/dist/three-pathfinding";

AFRAME.registerSystem("nav", {
  init: function() {
    this.pathfinder = new Pathfinding();
  },

  loadMesh: function(mesh, zone, applyWorldMatrix = false) {
    this.el.object3D.updateMatrixWorld();
    let geometry = mesh.geometry;
    if (applyWorldMatrix) {
      mesh.updateWorldMatrix(true, true);
      geometry = mesh.geometry.clone();
      try {
        geometry.applyMatrix4(mesh.matrixWorld);
      } catch (error) {
        getCurrentSettings().logger.warn(
          "nav::loadMesh - Could not properly initialized the nav mesh because of error",
          JSON.stringify(error, Object.getOwnPropertyNames(error))
        );
      }
    }
    this.pathfinder.setZoneData(zone, Pathfinding.createZone(geometry));
    const characterController = document.querySelector("[character-controller]");
    if (characterController) {
      characterController.components["character-controller"].resetPositionOnNavMesh(
        new THREE.Vector3(0, 0, 0),
        new THREE.Vector3(0, 0, 0),
        characterController.object3D
      );
    }
  }
});
