AFRAME.registerSystem("player-collision-system", {
  schema: {},
  init: function() {
    this.enableCollisions = false;
    this.playerRigPosition = new THREE.Vector3();
    this.objBox = new THREE.Box3();
    this.colliderBox = new THREE.Box3();
    this.currentCollisions = [];
    this.sceneColliders = [];
    this.activeSceneColliders = [];
    this.updateActiveCollidersList = this.updateActiveCollidersList.bind(this);
    // Active collider list will update after any object fades in or out
    this.el.addEventListener("fadeComplete", this.updateActiveCollidersList);
  },
  initCollisionSystem(showColliders, enableCollisions) {
    this.enableCollisions = enableCollisions;
    this.showColliders = showColliders;
    this.playerRig = document.querySelector("#player-rig");
  },
  registerCollider(object) {
    object.geometry.computeBoundingBox();
    this.sceneColliders.push(object);
    if (object.material) {
      if (this.showColliders) {
        object.material.opacity = 0.3;
        object.material.needsUpdate;
      } else {
        object.material.visible = false;
        object.material.needsUpdate;
      }
    }
    this.updateActiveCollidersList();
  },
  unregisterCollider(object) {
    this.sceneColliders = this.sceneColliders.filter(o => {
      return o.uuid !== object.uuid;
    });
  },
  updateActiveCollidersList() {
    // Remove colliders who have an invisible ancestor from the activeColliders list (collider itself is invisible)
    this.activeSceneColliders = this.sceneColliders.filter(col => {
      let visible = true;
      col.traverseAncestors(o => {
        if (!o.visible) visible = false;
      });
      return visible;
    });
  },
  // Scene object collider box intersects one of the player-collision-rig boxes
  intersectsRig(futurePlayerMatrix, objBox) {
    const playerColliderBoxes = this.playerRig.components["player-collision-rig"].colliderBoxes;
    for (const playerBox of playerColliderBoxes) {
      const playerBoxMesh = playerBox.getObject3D("mesh");

      if (!playerBoxMesh.geometry.boundingBox) {
        playerBoxMesh.geometry.computeBoundingBox();
      }
      this.colliderBox.copy(playerBoxMesh.geometry.boundingBox).applyMatrix4(futurePlayerMatrix);
      if (this.colliderBox.intersectsBox(objBox)) {
        return true;
      }
    }
    return false;
  },
  // Check for potential collisions against active scene objects
  checkCollisions(futurePlayerMatrix) {
    if (!this.playerRig || !this.playerRig.components["player-collision-rig"]) return false;
    if (!this.enableCollisions) return false;
    this.playerRigPosition.setFromMatrixPosition(futurePlayerMatrix);
    for (const obj of this.activeSceneColliders) {
      this.objBox.copy(obj.geometry.boundingBox).applyMatrix4(obj.matrixWorld);
      if (this.intersectsRig(futurePlayerMatrix, this.objBox)) {
        return true;
      }
    }
    return false;
  }
});
