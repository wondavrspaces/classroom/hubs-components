/**
 * Initializes teleport-controls when the environment bundle has loaded.
 * @namespace environment
 * @component nav-mesh-helper
 */
AFRAME.registerComponent("nav-mesh-helper", {
  schema: {
    teleportControls: { type: "selectorAll", default: "[teleport-controls]" },
    cursorTeleport: { type: "selector", default: "#cursor-controller" }
  },

  init: function() {
    const teleportControls = this.data.teleportControls;
    const cursorTeleport = this.data.cursorTeleport;
    this.el.addEventListener("bundleloaded", () => {
      if (!teleportControls && !cursorTeleport) return;

      for (let i = 0; i < teleportControls.length; i++) {
        teleportControls[i].components["teleport-controls"].queryCollisionEntities();
      }

      cursorTeleport.components["cursor-teleport"].updateRaycastObjects();
    });
  }
});
