/**
 * Instantiates GLTF models as specified in a bundle JSON.
 * @namespace gltf
 * @component gltf-bundle
 */
AFRAME.registerComponent("gltf-bundle", {
  schema: {
    src: { default: "" },
    resize: { type: "boolean", default: false },
    maxLength: { type: "number", default: 0.5 },
    isModelPlus: { type: "boolean", default: true },
    inflate: { type: "boolean", default: true },
    shouldComputeStats: { type: "boolean", default: false },
    useCache: { type: "boolean", default: false }
  },

  init: async function() {
    this._addGltfEntitiesForBundleJson = this._addGltfEntitiesForBundleJson.bind(this);
    this.onBundleLoad = this.onBundleLoad.bind(this);
  },

  update: async function(prevData) {
    if (prevData && prevData.src && prevData.src !== this.data.src) {
      this.el.setAttribute("scale", `1 1 1`);
      if (this.el.lastChild) {
        this.el.removeChild(this.el.lastChild); //remove gltf element
      }
    }
    this.baseURL = new URL(THREE.LoaderUtils.extractUrlBase(this.data.src), window.location.href);

    try {
      const res = await fetch(this.data.src);
      const data = await res.json();
      this._addGltfEntitiesForBundleJson(data);
    } catch (error) {
      this.el.emit("model-error", error);
    }
  },

  _addGltfEntitiesForBundleJson: function(bundleJson) {
    const loaded = [];

    if (this.data.resize) {
      this.el.object3D.visible = false;
    }

    for (let i = 0; i < bundleJson.assets.length; i++) {
      const asset = bundleJson.assets[i];

      // TODO: for now just skip resources, eventually we will want to hold on to a reference so that we can use them
      if (asset.type === "resource") continue;

      const src = new URL(asset.src, this.baseURL).href;
      const gltfEl = document.createElement("a-entity");
      if (this.data.isModelPlus) {
        gltfEl.setAttribute("gltf-model-plus", { src, useCache: this.data.useCache, inflate: this.data.inflate ,
          shouldComputeStats: this.data.shouldComputeStats});
      } else {
        gltfEl.setAttribute("gltf-model", `url(${src})`);
      }
      loaded.push(new Promise(resolve => gltfEl.addEventListener("model-loaded", resolve)));
      this.el.appendChild(gltfEl);
    }

    Promise.all(loaded).then(this.onBundleLoad);
  },
  remove: function() {
    this._addGltfEntitiesForBundleJson = this._addGltfEntitiesForBundleJson.bind(this);
  },
  onBundleLoad: function() {
    if (!this.data.resize) {
      this.el.emit("bundleloaded");
      return;
    }

    //We are computing the box of the object3D wrt. local coordinate and don't want to have world scale taken into account
    const parentObject3D = this.el.object3D.parent;
    if (parentObject3D) {
      parentObject3D.remove(this.el.object3D);
    }

    const boundingBox = new THREE.Box3().setFromObject(this.el.object3D);
    const width = boundingBox.max.x - boundingBox.min.x;
    const height = boundingBox.max.y - boundingBox.min.y;
    const depth = boundingBox.max.z - boundingBox.max.z;

    if (parentObject3D) {
      parentObject3D.add(this.el.object3D);
    }

    const aboveWidth = width - this.data.maxLength;
    const aboveHeight = height - this.data.maxLength;
    const aboveDepth = depth - this.data.maxLength;

    const lengths = [width, height, depth];
    const maxA = [aboveWidth, aboveHeight, aboveDepth];
    const maxValIndex = maxA.indexOf(Math.max(...maxA));
    if (maxA[maxValIndex] <= 0) {
      this.el.object3D.visible = true;
      this.el.emit("bundleloaded");
      return;
    }

    const ratio = this.data.maxLength / lengths[maxValIndex];
    this.el.setAttribute("scale", `${ratio} ${ratio} ${ratio}`);
    this.el.emit("bundleloaded");
    this.el.object3D.visible = true;
  }
});
