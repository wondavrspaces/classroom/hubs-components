/**
 * Cursor Teleport component for A-Frame.
 * Strongly inspired by https://github.com/mwbeene/aframe-cursor-teleport
 */
import { paths } from "../systems/userinput/paths";
import { sets } from "../systems/userinput/sets";

AFRAME.registerComponent("cursor-teleport", {
  schema: {
    cursorEl: { type: "selector", default: "" },
    playerRig: { type: "selector", default: "" },
    far: { type: "number", default: 10 },
    collisionEntities: { type: "string", default: "" },
    ignoreEntities: { type: "string", default: "" },
    landingMaxAngle: { default: "45", min: 0, max: 360 },
    landingNormal: { type: "vec3", default: "0 1 0" },
    canTeleport: { type: "boolean", default: true }
  },
  init: function() {
    // platform detect
    this.mobile = AFRAME.utils.device.isMobile();

    // main app
    this.scene = this.el.sceneEl;
    this.canvas = this.scene.renderer.domElement;

    // camera
    this.getHeadCamera();
    this.playerRigPos = this.data.playerRig.object3D.position;
    //collision
    this.rayCaster = new THREE.Raycaster();
    this.rayCaster.far = this.data.far;
    // Three mesh bvh optimization - Could be activated because we are only using the first object intersected
    this.rayCaster.firstHitOnly = true;
    this.referenceNormal = new THREE.Vector3();
    this.rayCastObjects = [];

    // Update collision normal
    this.referenceNormal.copy(this.data.landingNormal);

    // teleport indicator
    const geo = new THREE.RingGeometry(0.26, 0.265, 32, 1);
    geo.rotateX(-Math.PI / 2);
    geo.translate(0, 0.02, 0);
    const mat = new THREE.MeshBasicMaterial();
    mat.transparent = true;
    mat.opacity = 0.75;
    this.teleportIndicator = new THREE.Mesh(geo, mat);
    this.scene.object3D.add(this.teleportIndicator);

    // transition
    this.transitioning = false;
    this.transitionProgress = 0;
    this.transitionSpeed = 0.01;
    this.transitionCamPosStart = new THREE.Vector3();
    this.transitionCamPosEnd = new THREE.Vector3();

    this.updateRaycastObjects();

    // event listeners
    this.canvas.addEventListener("mousedown", this.mouseDown.bind(this), false);
    this.canvas.addEventListener("mouseup", this.mouseUp.bind(this), false);
    this.canvas.addEventListener("touchstart", this.mouseDown.bind(this), false);
    this.canvas.addEventListener("touchend", this.mouseUp.bind(this), false);

    // helper functions
    this.easeInOutQuad = function(t) {
      return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
    };
  },
  remove() {
    this.scene.object3D.remove(this.teleportIndicator);
    this.canvas.removeEventListener("mousedown", this.mouseDown.bind(this), false);
    this.canvas.removeEventListener("mouseup", this.mouseUp.bind(this), false);
    this.canvas.removeEventListener("touchstart", this.mouseDown.bind(this), false);
    this.canvas.removeEventListener("touchend", this.mouseUp.bind(this), false);
  },
  tick: function() {
    const userinput = AFRAME.scenes[0].systems.userinput;
    if (this.isCheckingValidity) {
      if (userinput && (
        userinput.activeSets.has(sets.cursorHoldingPen) ||
        userinput.activeSets.has(sets.cursorHoldingInteractable) ||
        userinput.activeSets.has(sets.cursorHoldingUI))
      ) {
        this.isUnvalidated = true;
      }

      const cameraRotation = userinput.get(paths.actions.cameraDelta);
      const isRotatingCamera = cameraRotation && (cameraRotation[0] !== 0 || cameraRotation[1] !== 0);
      if (isRotatingCamera) {
        this.isUnvalidated = true;
      }

      this.isCheckingValidity = !this.isUnvalidated;
    }
    // No teleport indicator when grabbing or using pen or other specific action
    if (
      !this.data.canTeleport ||
      (userinput &&
        (userinput.activeSets.has(sets.cursorHoldingPen) ||
          userinput.activeSets.has(sets.cursorHoldingInteractable) ||
          userinput.activeSets.has(sets.cursorHoveringOnInteractable) ||
          userinput.activeSets.has(sets.cursorHoldingUI) ||
          userinput.activeSets.has(sets.cursorHoveringOnUI)) &&
        !this.transitioning)
    ) {
      this.active = false;
      this.teleportIndicator.visible = false;
      this.transitioning = false;
      return;
    }

    if (!this.transitioning && !this.mobile) {
      const pos = this.getTeleportPosition();
      if (!this.mobile) {
        if (pos) {
          this.active = true;
          this.teleportIndicator.visible = true;
          this.teleportIndicator.position.x = pos.x;
          this.teleportIndicator.position.y = pos.y;
          this.teleportIndicator.position.z = pos.z;
        } else {
          this.active = false;
          this.teleportIndicator.visible = false;
        }
      }
    }
    if (this.transitioning) {
      this.transitionProgress += this.transitionSpeed;

      // set camera position
      this.playerRigPos.x =
        this.transitionCamPosStart.x +
        (this.transitionCamPosEnd.x - this.transitionCamPosStart.x) * this.easeInOutQuad(this.transitionProgress);
      this.playerRigPos.y =
        this.transitionCamPosStart.y +
        (this.transitionCamPosEnd.y - this.transitionCamPosStart.y) * this.easeInOutQuad(this.transitionProgress);
      this.playerRigPos.z =
        this.transitionCamPosStart.z +
        (this.transitionCamPosEnd.z - this.transitionCamPosStart.z) * this.easeInOutQuad(this.transitionProgress);

      if (this.transitionProgress >= 1) {
        this.transitioning = false;
        const teleportEventDetail = {
          oldPosition: this.transitionCamPosStart,
          newPosition: this.transitionCamPosEnd,
          hitPoint: new THREE.Vector3(
            this.teleportIndicator.position.x,
            this.teleportIndicator.position.y,
            this.teleportIndicator.position.z
          )
        };
        this.el.emit("teleported", teleportEventDetail);
      }
    }
  },
  getHeadCamera: function() {
    const playerRigEl = this.data.playerRig;
    this.cam = playerRigEl.querySelector(".avatar-head");
  },
  setDefaultCollisionPlane: function() {
    const geo = new THREE.PlaneGeometry(50, 50, 1);
    geo.rotateX(-Math.PI / 2);
    const mat = new THREE.MeshNormalMaterial();
    const collisionMesh = new THREE.Mesh(geo, mat);
    // mark this mesh as a collision object
    collisionMesh.userData.collision = true;
    this.rayCastObjects.push(collisionMesh);
  },
  updateRaycastObjects: function() {
    // updates the array of meshes we will need to raycast to
    // clear the array of any existing meshes
    this.rayCastObjects = [];

    if (this.data.collisionEntities != "") {
      // traverse collision entities and add their meshes to the rayCastEntities array.
      const collisionEntities = this.scene.querySelectorAll(this.data.collisionEntities);

      collisionEntities.forEach(e => {
        e.object3D.traverse(child => {
          if (child instanceof THREE.Mesh) {
            // mark this mesh as a collision object
            child.userData.collision = true;
            this.rayCastObjects.push(child);
          }
        });
      });
      // if no collision entities are found in the scene, create a default ground plane collision.
      if (this.rayCastObjects.length === 0) {
        this.setDefaultCollisionPlane();
      }
    } else {
      // if no collision entities are specified, create a default ground plane collision.
      this.setDefaultCollisionPlane();
    }

    // We may need some entities to be seen by the raycaster even though they are not teleportable.
    // This prevents the user from unnesserily teleporting when clicking things like buttons or UI.

    if (this.data.ignoreEntities != "") {
      const ignoreEntities = this.scene.querySelectorAll(this.data.ignoreEntities);
      ignoreEntities.forEach(e => {
        e.object3D.traverse(child => {
          if (child instanceof THREE.Mesh) {
            this.rayCastObjects.push(child);
          }
        });
      });
    }
  },
  getTeleportPosition: function() {
    this.updateRaycastObjects();
    if (this.rayCastObjects.length != 0) {
      if (!this.cam || !this.cam.object3D) {
        return false;
      }
      const origin = this.cam.object3D.getWorldPosition();
      const pose = this.data.cursorEl.object3D.position;
      const direction = new THREE.Vector3().subVectors(pose, origin).normalize();
      if (!pose || !direction) {
        return false;
      }

      this.rayCaster.set(origin, direction);

      const intersects = this.rayCaster.intersectObjects(this.rayCastObjects);

      if (intersects.length != 0 && this.isValidNormalsAngle(intersects[0].face.normal)) {
        if (intersects[0].object.userData.collision == true) {
          return intersects[0].point;
        }
        return false;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
  isValidNormalsAngle: function(collisionNormal) {
    const angleNormals = this.referenceNormal.angleTo(collisionNormal);
    return THREE.MathUtils.RAD2DEG * angleNormals <= this.data.landingMaxAngle;
  },

  mouseDown: function() {
    this.isCheckingValidity = true;
    this.isUnvalidated = false;
  },

  mouseUp: function() {
    this.isCheckingValidity = false;

    if (!this.data.canTeleport || this.isUnvalidated) {
      this.isUnvalidated = false;
      return;
    }
    const pos = this.getTeleportPosition();
    if (pos) {
      this.teleportIndicator.position.x = pos.x;
      this.teleportIndicator.position.y = pos.y;
      this.teleportIndicator.position.z = pos.z;
      this.transition(pos);
    }
  },
  transition: function(destPos) {
    this.transitionProgress = 0;

    this.transitionCamPosEnd.x = destPos.x;
    this.transitionCamPosEnd.y = destPos.y;
    this.transitionCamPosEnd.z = destPos.z;

    this.transitionCamPosStart.x = this.playerRigPos.x;
    this.transitionCamPosStart.y = this.playerRigPos.y;
    this.transitionCamPosStart.z = this.playerRigPos.z;

    this.transitioning = true;
  }
});
