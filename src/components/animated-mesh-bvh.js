import {
  StaticGeometryGenerator,
  computeBoundsTree,
  disposeBoundsTree,
  CENTER,
  MeshBVH,
  acceleratedRaycast
} from "three-mesh-bvh";
THREE.BufferGeometry.prototype.computeBoundsTree = computeBoundsTree;
THREE.BufferGeometry.prototype.disposeBoundsTree = disposeBoundsTree;

const materialProperties = {
  visible: false
};

const debugMaterialProperties = {
  wireframe: true,
  transparent: true,
  opacity: 0.2
};

AFRAME.registerComponent("animated-mesh-bvh", {
  schema: {
    debug: {
      type: "boolean",
      default: false
    }
  },

  generators: [],
  meshHelpers: [],
  skinnedMeshes: [],

  isActive: false,
  hasRun: false,

  init() {
    this.onBundleLoaded = this.onBundleLoaded.bind(this);
    this.el.parentEl.addEventListener("bundleloaded", this.onBundleLoaded);
  },

  remove() {
    this.el.parentEl.removeEventListener("bundleloaded", this.onBundleLoaded);
    this.generators = [];
    this.meshHelpers = [];
    this.skinnedMeshes = [];
  },

  onBundleLoaded() {
    this.activate();
  },

  activate() {
    this.isActive = true;

    this.el.object3D.traverse(o => {
      if (o instanceof THREE.SkinnedMesh) {
        this.attachGenerator(o);
      }
    });
  },

  attachGenerator(skinnedMesh) {
    THREE.Mesh.prototype.raycast = acceleratedRaycast;

    const generator = new StaticGeometryGenerator(skinnedMesh);
    generator.applyWorldTransforms = false;

    const material = new THREE.MeshBasicMaterial(this.data.debug ? debugMaterialProperties : materialProperties);

    const meshHelper = new THREE.Mesh(new THREE.BufferGeometry(), material);
    if (this.data.debug) meshHelper.frustumCulled = false;

    this.generators.push(generator);
    this.meshHelpers.push(meshHelper);
    this.skinnedMeshes.push(skinnedMesh);

    skinnedMesh.add(meshHelper);
  },

  async tick() {
    if (!this.isActive) return;
    if (this.hasRun) return;

    // We need to wait or the transform are broken.
    await setTimeout(() => {}, 0);

    this.generators.map((generator, index) => generator.generate(this.meshHelpers[index].geometry));
    this.meshHelpers.map((meshHelper, index) => {
      if (!meshHelper.geometry.boundsTree) {
        meshHelper.geometry.computeBoundsTree();
        meshHelper.geometry.computeBoundingBox();
        this.skinnedMeshes[index].geometry.boundsTree = new MeshBVH(meshHelper.geometry, {
          strategy: CENTER,
          maxDepth: 30
        });
      }
    });
    this.hasRun = true;
  }
});
