import { getCurrentSettings } from "../utils/settings";
import { QUALITY_SETTINGS } from "../utils/constants";

/**
 * Hides entities based on the scene's quality mode
 * @namespace environment
 * @component hide-when-quality
 */
AFRAME.registerComponent("hide-when-quality", {
  schema: { type: "string", default: QUALITY_SETTINGS.LOW },

  init() {
    this.onQualityChanged = this.onQualityChanged.bind(this);
    this.el.sceneEl.addEventListener("quality-changed", this.onQualityChanged);
  },

  onQualityChanged(event) {
    this.updateComponentState(event.detail);
  },

  update(oldData) {
    if (this.data !== oldData) {
      this.updateComponentState(getCurrentSettings().quality);
    }
  },

  updateComponentState(quality) {
    this.el.setAttribute("visible", quality !== this.data);
  }
});
