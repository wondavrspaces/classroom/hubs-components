AFRAME.registerComponent("visibility-by-path", {
  schema: {
    path: { type: "string" }
  },
  tick() {
    const userinput = AFRAME.scenes[0].systems.userinput;
    const shouldBeVisible = !!userinput.get(this.data.path);
    if (true) {
      this.el.setAttribute("visible", shouldBeVisible);
    }
  }
});
