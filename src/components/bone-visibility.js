/**
 * Scales an object to near-zero if the object is invisible. Useful for bones representing avatar body parts.
 * @namespace avatar
 * @component bone-visibility
 */
AFRAME.registerComponent("bone-visibility", {
  schema: {
    scale: { type: "number", default: 1 }
  },
  tick() {
    const { visible } = this.el.object3D;
    // @TODO check when ready player me fixes its issue with avatar generation if we can rollback to apply change only when visibility changes
    if (visible) {
      this.el.object3D.scale.set(this.data.scale, this.data.scale, this.data.scale);
    } else {
      // Three.js doesn't like updating matrices with 0 scale, so we set it to a near zero number.
      this.el.object3D.scale.set(0.00000001, 0.00000001, 0.00000001);
    }

    this.el.object3D.updateMatrices(true, true);
    this.el.object3D.updateMatrixWorld(true, true);
  }
});
