// Will create a player rig of octogonal (or 16/32 sided) base, made of rotated boxes

AFRAME.registerComponent("player-collision-rig", {
  schema: {
    debug: { type: "boolean", default: false },
    rigPosition: { type: "vec3", default: { x: 0, y: 0.8, z: 0 } },
    rigHeight: { type: "number", default: 1.6 },
    rigRadius: {
      type: "number",
      default: 0.05
    },
    numfaces: {
      type: "number",
      default: 8,
      oneOf: [8, 16, 32]
    }
  },
  init() {
    this.colliderBoxes = [];
    this.colliderContainer = document.createElement("a-entity");
    this.colliderContainer.id = "player-rig-collider-container";
    this.colliderContainer.setAttribute("position", this.data.rigPosition);
    this.el.appendChild(this.colliderContainer);
    const faceAngle = 360 / this.data.numfaces;
    const faceLength = 2 * this.data.rigRadius * Math.sin((faceAngle / 2) * (Math.PI / 180));
    for (let i = 0; i < this.data.numfaces / 2; i++) {
      const colliderBox = document.createElement("a-entity");
      colliderBox.setAttribute("geometry", {
        primitive: "box",
        height: this.data.rigHeight,
        width: faceLength,
        depth: 2 * this.data.rigRadius
      });
      // Debug mode makes the player collision rig visible
      colliderBox.setAttribute("material", {
        visible: this.data.debug,
        wireframe: this.data.debug
      });
      colliderBox.setAttribute("rotation", {
        x: 0,
        y: i * faceAngle,
        z: 0
      });
      this.colliderBoxes.push(colliderBox);
      this.colliderContainer.appendChild(colliderBox);
    }
  },
  remove() {
    for (const box of this.colliderBoxes) {
      this.colliderContainer.removeChild(box);
    }
    this.el.removeChild(this.colliderContainer);
    this.colliderBoxes.length = 0;
  }
});
