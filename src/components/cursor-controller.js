import { paths } from "../systems/userinput/paths";
import { getLastWorldPosition } from "../utils/three-utils";

const CURSOR_INTERSECTION_OFFSET = 0.05;
/**
 * Manages targeting and physical cursor location. Has the following responsibilities:
 *
 * - Tracking which entities in the scene can be targeted by the cursor (`objects`).
 * - Performing a raycast per-frame or on-demand to identify which entity is being currently targeted.
 * - Updating the visual presentation and position of the `cursor` entity and `line` component per frame.
 * - Sending an event when an entity is targeted or un-targeted.
 */
AFRAME.registerComponent("cursor-controller", {
  schema: {
    cursor: { type: "selector" },
    camera: { type: "selector" },
    far: { default: 3 },
    near: { default: 0.06 },
    cursorDistance: { default: 3 },
    cursorColorHovered: { default: "#2F80ED" },
    cursorColorUnhovered: { default: "#FFFFFF" },
    rayObject: { type: "selector" },
    objects: { default: "" },
    //SceneEl is the target object of all queries per default. Defining a targetObject will restrict queries to this element.
    targetObject: { type: "selector" },
    //A class can be looked for on intersected object to prioritize them from other element
    overridingIntersectPriorityClass: { type: "string", default: "" }
  },

  init: function() {
    this.enabled = true;

    this.data.cursor.addEventListener(
      "loaded",
      () => {
        if (window && window.APP && window.APP.RENDER_ORDER) {
          this.data.cursor.object3DMap.mesh.renderOrder = window.APP.RENDER_ORDER.CURSOR;
        }
      },
      { once: true }
    );

    // raycaster state
    this.setDirty = this.setDirty.bind(this);
    this.targets = [];
    this.raycaster = new THREE.Raycaster();
    //Mesh BVH specific method
    this.raycaster.firstHitOnly = true;
    this.dirty = true;
    this.distance = this.data.far;
    const lineMaterial = new THREE.LineBasicMaterial({
      color: "white",
      opacity: 0.2,
      transparent: true,
      visible: false
    });

    const lineGeometry = new THREE.BufferGeometry();
    lineGeometry.setAttribute("position", new THREE.BufferAttribute(new Float32Array(2 * 3), 3));

    this.line = new THREE.Line(lineGeometry, lineMaterial);
    this.el.setObject3D("line", this.line);

    this.nightModeSystem = this.el.sceneEl.systems["night-mode-manager"];
  },

  update: function(oldData) {
    this.raycaster.far = this.data.far;
    this.raycaster.near = this.data.near;
    if (oldData.targetObject !== this.data.targetObject) {
      const oldObservedElement = this.getTargetObject(oldData.targetObject);
      this.removeObserver(oldObservedElement);

      const nwObservedElement = this.getTargetObject(this.data.targetObject);
      this.initObserver(nwObservedElement);
    }
    this.setDirty();
  },

  play: function() {
    this.initObserver(this.getTargetObject(this.data.targetObject));
  },

  initObserver: function(observedElement) {
    this.observer = new MutationObserver(this.setDirty);
    this.observer.observe(observedElement, { childList: true, attributes: true, subtree: true });
    observedElement.addEventListener("object3dset", this.setDirty);
    observedElement.addEventListener("object3dremove", this.setDirty);
  },

  removeObserver: function(observedElement) {
    if (!this.observer) return;

    this.observer.disconnect();
    observedElement.removeEventListener("object3dset", this.setDirty);
    observedElement.removeEventListener("object3dremove", this.setDirty);
    this.observer = null;
  },

  pause: function() {
    this.removeObserver(this.getTargetObject(this.data.targetObject));
  },

  setDirty: function() {
    this.dirty = true;
  },

  populateEntities: function(selector, target) {
    target.length = 0;
    if (!selector) return;
    const els = this.getTargetObject(this.data.targetObject).querySelectorAll(selector);
    for (let i = 0; i < els.length; i++) {
      if (els[i].object3D) {
        target.push(els[i].object3D);
      }
    }
  },

  emitIntersectionEvents: function(prevIntersection, currIntersection) {
    // if we are now intersecting something, and previously we were intersecting nothing or something else
    if (currIntersection && (!prevIntersection || currIntersection.object.el !== prevIntersection.object.el)) {
      this.data.cursor.emit("raycaster-intersection", { el: currIntersection.object.el });
    }
    // if we were intersecting something, but now we are intersecting nothing or something else
    if (prevIntersection && (!currIntersection || currIntersection.object.el !== prevIntersection.object.el)) {
      this.data.cursor.emit("raycaster-intersection-cleared", { el: prevIntersection.object.el });
    }
  },

  tick: (() => {
    const rawIntersections = [];
    const cameraPos = new THREE.Vector3();

    return function() {
      if (this.dirty) {
        // app aware devices cares about this.targets so we must update it even if cursor is not enabled
        this.populateEntities(this.data.objects, this.targets);
        this.dirty = false;
      }

      const userinput = AFRAME.scenes[0].systems.userinput;
      const cursorPose = userinput.get(paths.actions.cursor.pose);
      const rightHandPose = userinput.get(paths.actions.rightHand.pose);

      this.data.cursor.object3D.visible = this.enabled && !!cursorPose;
      this.line.material.visible = !!(this.enabled && rightHandPose);

      if (!this.enabled || !cursorPose) {
        return;
      }

      let intersection;
      const isGrabbing = this.data.cursor.components["super-hands"].state.has("grab-start");
      if (!isGrabbing) {
        rawIntersections.length = 0;
        this.raycaster.ray.origin = cursorPose.position;
        this.raycaster.ray.direction = cursorPose.direction;
        this.raycaster.intersectObjects(this.targets, true, rawIntersections);
        intersection = rawIntersections.filter(x => x.object.el);
        const overridingPriorityIntersection = intersection.find(
          x =>
            x.object.el &&
            x.object.el.classList &&
            x.object.el.classList.contains(this.data.overridingIntersectPriorityClass)
        );
        if (overridingPriorityIntersection) {
          intersection = overridingPriorityIntersection;
        } else {
          intersection = rawIntersections.find(x => x.object.el);
        }
        this.emitIntersectionEvents(this.prevIntersection, intersection);
        this.prevIntersection = intersection;
        // Add intersection offset to avoid drawing inside objects in pen mode
        this.distance = intersection ? intersection.distance - CURSOR_INTERSECTION_OFFSET : this.data.cursorDistance;
      }

      const { cursor, near, far, camera, cursorColorHovered, cursorColorUnhovered } = this.data;

      const cursorModDelta = userinput.get(paths.actions.cursor.modDelta);
      if (isGrabbing && cursorModDelta) {
        this.distance = THREE.MathUtils.clamp(this.distance - cursorModDelta, near, far);
      }
      cursor.object3D.position.copy(cursorPose.position).addScaledVector(cursorPose.direction, this.distance);
      // The cursor will always be oriented towards the player about its Y axis, so objects held by the cursor will rotate towards the player.
      getLastWorldPosition(camera.object3D, cameraPos);
      cameraPos.y = cursor.object3D.position.y;
      cursor.object3D.lookAt(cameraPos);
      cursor.object3D.scale.setScalar(Math.pow(this.distance, 0.315) * 0.75);
      cursor.object3D.matrixNeedsUpdate = true;
      const isHoveringInput = this.isInput(intersection);
      const isHovering = this.isInteractable(intersection);
      const isRotatingCamera = userinput.get(paths.actions.cameraDelta);

      // VR cursor (ball) management
      const cursorColor = isHovering || isGrabbing ? cursorColorHovered : cursorColorUnhovered;
      if (this.data.cursor.components.material.data.color !== cursorColor) {
        this.data.cursor.setAttribute("material", "color", cursorColor);
        this.data.cursor.setAttribute("material", "shader", "flat");
      }

      // Desktop cursor Management
      const canvasElement = document.querySelector(".a-canvas");
      if (isGrabbing || isRotatingCamera) {
        if (canvasElement.dataset.cursormode !== "grabbing") canvasElement.dataset.cursormode = "grabbing";
      } else if (isHoveringInput) {
        if (canvasElement.dataset.cursormode !== "input") canvasElement.dataset.cursormode = "input";
      } else if (isHovering) {
        if (canvasElement.dataset.cursormode !== "pointer") canvasElement.dataset.cursormode = "pointer";
      } else {
        if (canvasElement.dataset.cursormode !== "grab") canvasElement.dataset.cursormode = "grab";
      }
      if (this.line.material.visible) {
        // Reach into line component for better performance
        const posePosition = cursorPose.position;
        const cursorPosition = cursor.object3D.position;
        const positionArray = this.line.geometry.attributes.position.array;

        positionArray[0] = posePosition.x;
        positionArray[1] = posePosition.y;
        positionArray[2] = posePosition.z;
        positionArray[3] = cursorPosition.x;
        positionArray[4] = cursorPosition.y;
        positionArray[5] = cursorPosition.z;

        this.line.geometry.attributes.position.needsUpdate = true;
        this.line.geometry.computeBoundingSphere();
      }
    };
  })(),
  isInput(intersection) {
    if (!intersection) return false;
    return intersection.object.el.classList.contains("vr-input");
  },
  isInteractable: function(intersection) {
    function hasInteractableClass(el, nightModeSystem) {
      return (
        el.classList.contains("ui") ||
        el.classList.contains("interactable") ||
        el.classList.contains("actionable") ||
        (el.classList.contains("night-mode-actionable") && nightModeSystem && nightModeSystem.isActive)
      );
    }
    if (!intersection) return false;
    // Find if intersection is child of an interactive-element-loader (dyn annotation) that has the interactable class
    const closest = intersection.object.el.closest("[interactive-element-loader]");
    return (
      hasInteractableClass(intersection.object.el, this.nightModeSystem) ||
      (closest && hasInteractableClass(closest, this.nightModeSystem))
    );
  },
  remove: function() {
    this.emitIntersectionEvents(this.prevIntersection, null);
    delete this.prevIntersection;
  },
  getTargetObject: function(targetObject) {
    return targetObject || this.el.sceneEl;
  }
});
