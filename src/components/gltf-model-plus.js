import nextTick from "../utils/next-tick";
import { mapMaterials } from "../utils/material-utils";
import MobileStandardMaterial from "../materials/MobileStandardMaterial";
import { disposeNode, cloneObject3D } from "../utils/three-utils";
import { QUALITY_SETTINGS } from "../utils/constants";
import { getCurrentSettings, isNightModeEnabled } from "../utils/settings";
import { GLTFLoader } from "../loaders/GLTFLoader";
import {
  TextureLightBasicMaterial,
  TextureLightStandardMaterial,
  TEXTURE_LIGHT_SYSTEM_NAME
} from "@wondavrspaces/texture-light";
import { generateMeshBVH } from "../utils/generateMeshBVH";
import "../utils/basis/KTX2Loader";
import { computeStats } from "../utils/gltf-inspect-utils";
const COLLIDER_PREFIX = "Wonda_character_collider";

let gltfLoader;
export function getGLTFLoader() {
  return gltfLoader;
}
//We are using this method to init the gltfLoader only once
export function initializeGLTFLoader(renderer) {
  if (!gltfLoader) {
    gltfLoader = new GLTFLoader();
    gltfLoader.register(parser => new GLTFHubsLightMapExtension(parser));

    const ktx2Loader = new THREE.KTX2Loader();
    ktx2Loader.setTranscoderPath("assets/js/basis/");
    ktx2Loader.detectSupport(renderer);

    const dracoLoader = new THREE.DRACOLoader();
    dracoLoader.setDecoderPath("/assets/js/draco/");
    dracoLoader.preload();

    gltfLoader.setKTX2Loader(ktx2Loader);
    gltfLoader.setDRACOLoader(dracoLoader);
  }
  return gltfLoader;
}

function hasStats(gltf) {
  return !gltf.stats;
}

class GLTFCache {
  constructor() {
    this.cache = new Map();
  }

  set(src, gltf) {
    this.cache.set(src, {
      gltf,
      count: 0
    });
    return this.retain(src);
  }

  has(src) {
    return this.cache.has(src);
  }

  get(src) {
    return this.cache.get(src);
  }

  retain(src) {
    const cacheItem = this.cache.get(src);
    cacheItem.count++;
    return cacheItem;
  }

  release(src) {
    const cacheItem = this.cache.get(src);

    if (!cacheItem) {
      console.error(`Releasing uncached gltf ${src}`);
      return;
    }

    cacheItem.count--;
    if (cacheItem.count <= 0) {
      cacheItem.gltf.scene.traverse(disposeNode);
      this.cache.delete(src);
    }
  }
}
const gltfCache = new GLTFCache();
const inflightGltfsWithCache = new Map();
const inflightGltfs = new Map();

function defaultInflator(el, componentName, componentData) {
  if (!AFRAME.components[componentName]) {
    getCurrentSettings().logger.warn(`Inflator failed. "${componentName}" component does not exist.`);
    return;
  }
  if (AFRAME.components[componentName].multiple && Array.isArray(componentData)) {
    for (let i = 0; i < componentData.length; i++) {
      el.setAttribute(componentName + "__" + i, componentData[i]);
    }
  } else {
    el.setAttribute(componentName, componentData);
  }
}

function tryInstanciateWondaNavMesh(element) {
  const wondaNavMesh = element.querySelector(".wonda_navmesh");
  if (!wondaNavMesh) return;

  const object3DTarget = wondaNavMesh.object3D;
  const nav = AFRAME.scenes[0].systems.nav;
  const zone = "character";
  let found = false;
  object3DTarget.traverse(object => {
    if (object.isMesh && !found) {
      found = true;
      if (nav) nav.loadMesh(object, zone, true);
      object.parent.visible = false;
    }
  });

  wondaNavMesh.setAttribute("nav-mesh", {});
}
// Find potential collider objects and register them in the collision system
function instanciatePlayerCollider(object3D) {
  const playerCollisionSystem = AFRAME.scenes[0].systems["player-collision-system"];
  if (!playerCollisionSystem) return;
  object3D.traverse(o => {
    if (o.name.includes(COLLIDER_PREFIX)) {
      playerCollisionSystem.registerCollider(o);
    }
  });
}
function handleMaterial(material, preferredTechnique) {
  if (material.isMeshStandardMaterial && preferredTechnique === "KHR_materials_unlit") {
    return MobileStandardMaterial.fromMeshStandardMaterial(material);
  }

  return material;
}

function handleMaterialWithTextureLight(material, textureLightSystem, preferredTechnique) {
  let nwMaterial;
  if (material.isMeshStandardMaterial && preferredTechnique === "KHR_materials_unlit") {
    const basicMaterial = MobileStandardMaterial.fromMeshStandardMaterial(material);
    nwMaterial = new TextureLightBasicMaterial(textureLightSystem.getTextureLight());
    nwMaterial.fromBasicMaterial(basicMaterial);
    nwMaterial.roughness = material.roughness;
    nwMaterial.roughnessMap = material.roughnessMap;
    // if (nwMaterial.roughness === 1) nwMaterial.roughness = 0.1;
  } else if (material.isMeshBasicMaterial) {
    nwMaterial = new TextureLightBasicMaterial(textureLightSystem.getTextureLight());
    nwMaterial.fromBasicMaterial(material);
  } else if (material.isMeshStandardMaterial) {
    nwMaterial = new TextureLightStandardMaterial(textureLightSystem.getTextureLight());
    nwMaterial.fromStandardMaterial(material);
  } else {
    nwMaterial = material;
  }
  return nwMaterial;
}

AFRAME.GLTFModelPlus = {
  // eslint-disable-next-line no-unused-vars
  components: {},
  registerComponent(componentKey, componentName, inflator) {
    inflator = inflator || defaultInflator;
    AFRAME.GLTFModelPlus.components[componentKey] = { inflator, componentName };
  }
};

function cloneGltf(gltf) {
  return {
    animations: gltf.scene.animations,
    scene: cloneObject3D(gltf.scene),
    stats: gltf.stats
  };
}

function getHubsComponents(node) {
  const hubsComponents =
    node.userData.gltfExtensions &&
    (node.userData.gltfExtensions.MOZ_hubs_components || node.userData.gltfExtensions.HUBS_components);

  // We can remove support for legacy components when our environment, avatar and interactable models are
  // updated to match Spoke output.
  const legacyComponents = node.userData.components;

  return hubsComponents || legacyComponents;
}

/// Walks the tree of three.js objects starting at the given node, using the GLTF data
/// and template data to construct A-Frame entities and components when necessary.
/// (It's unnecessary to construct entities for subtrees that have no component data
/// or templates associated with any of their nodes.)
///
/// Returns the A-Frame entity associated with the given node, if one was constructed.
const inflateEntities = function (indexToEntityMap, node, templates, isRoot, modelToWorldScale = 1) {
  // TODO: Remove this once we update the legacy avatars to the new node names
  if (node.name === "Chest") {
    node.name = "Spine";
  } else if (node.name === "Root Scene") {
    node.name = "AvatarRoot";
  } else if (node.name === "Bot_Skinned") {
    node.name = "AvatarMesh";
  }

  // inflate subtrees first so that we can determine whether or not this node needs to be inflated
  const childEntities = [];
  const children = node.children.slice(0); // setObject3D mutates the node's parent, so we have to copy
  for (const child of children) {
    const el = inflateEntities(indexToEntityMap, child, templates);
    if (el) {
      childEntities.push(el);
    }
  }

  const entityComponents = getHubsComponents(node);

  const nodeHasBehavior = !!entityComponents || node.name in templates;
  if (!nodeHasBehavior && !childEntities.length && !isRoot && node.name.toLowerCase() !== "wonda_navmesh") {
    return null; // we don't need an entity for this node
  }

  const el = document.createElement("a-entity");
  el.append.apply(el, childEntities);

  // Remove invalid CSS class name characters.
  const className = (node.name || node.uuid).replace(/[^\w-]/g, "");
  if (node.name.toLowerCase() === "wonda_navmesh") {
    el.classList.add("wonda_navmesh");
  } else {
    el.classList.add(className);
  }

  // AFRAME rotation component expects rotations in YXZ, convert it
  if (node.rotation.order !== "YXZ") {
    node.rotation.setFromQuaternion(node.quaternion, "YXZ");
  }

  // Copy over the object's transform to the THREE.Group and reset the actual transform of the Object3D
  // all updates to the object should be done through the THREE.Group wrapper
  el.object3D.position.copy(node.position);
  el.object3D.rotation.copy(node.rotation);
  el.object3D.scale.copy(node.scale).multiplyScalar(modelToWorldScale);
  el.object3D.matrixNeedsUpdate = true;

  node.matrixAutoUpdate = false;
  node.matrix.identity();
  node.matrix.decompose(node.position, node.rotation, node.scale);

  el.setObject3D(node.type.toLowerCase(), node);
  if (entityComponents && "nav-mesh" in entityComponents) {
    el.setObject3D("mesh", node);
  }

  // Set the name of the `THREE.Group` to match the name of the node,
  // so that templates can be attached to the correct AFrame entity.
  el.object3D.name = node.name;

  // Set the uuid of the `THREE.Group` to match the uuid of the node,
  // so that `THREE.PropertyBinding` will find (and later animate)
  // the group. See `PropertyBinding.findNode`:
  // https://github.com/mrdoob/three.js/blob/dev/src/animation/PropertyBinding.js#L211
  el.object3D.uuid = node.uuid;
  node.uuid = THREE.MathUtils.generateUUID();

  if (node.animations) {
    // Pass animations up to the group object so that when we can pass the group as
    // the optional root in `THREE.AnimationMixer.clipAction` and use the hierarchy
    // preserved under the group (but not the node). Otherwise `clipArray` will be
    // `null` in `THREE.AnimationClip.findByName`.
    node.parent.animations = node.animations;
  }

  const gltfIndex = node.userData.gltfIndex;
  if (gltfIndex !== undefined) {
    indexToEntityMap[gltfIndex] = el;
  }

  return el;
};

async function inflateComponents(inflatedEntity, indexToEntityMap) {
  let isFirstInflation = true;
  const objectInflations = [];

  inflatedEntity.object3D.traverse(async object3D => {
    const objectInflation = {};
    objectInflation.promise = new Promise(resolve => (objectInflation.resolve = resolve));
    objectInflations.push(objectInflation);

    if (!isFirstInflation) {
      await objectInflations.shift().promise;
    }
    isFirstInflation = false;

    const entityComponents = getHubsComponents(object3D);
    const el = object3D.el;

    if (entityComponents && el) {
      for (const prop in entityComponents) {
        if (entityComponents.hasOwnProperty(prop) && AFRAME.GLTFModelPlus.components.hasOwnProperty(prop)) {
          const { componentName, inflator } = AFRAME.GLTFModelPlus.components[prop];
          await inflator(el, componentName, entityComponents[prop], entityComponents, indexToEntityMap);
        }
      }
    }

    objectInflation.resolve();
  });

  await objectInflations.shift().promise;
}

function attachTemplate(root, name, templateRoot) {
  const targetEls = root.querySelectorAll("." + name);
  for (const el of targetEls) {
    const root = templateRoot.cloneNode(true);
    // Merge root element attributes with the target element
    for (const { name, value } of root.attributes) {
      el.setAttribute(name, value);
    }

    // Append all child elements
    while (root.children.length > 0) {
      el.appendChild(root.children[0]);
    }
  }
}

function runMigration(version, json) {
  if (version < 2) {
    if (!json.nodes) return;
    //old heightfields will be on the same node as the nav-mesh, delete those
    const oldHeightfieldNode = json.nodes.find(node => {
      let components = null;
      if (node.extensions && node.extensions.MOZ_hubs_components) {
        components = node.extensions.MOZ_hubs_components;
      } else if (node.extensions && node.extensions.HUBS_components) {
        components = node.extensions.HUBS_components;
      }
      return components && components.heightfield && components["nav-mesh"];
    });
    if (oldHeightfieldNode) {
      if (oldHeightfieldNode.extensions.MOZ_hubs_components) {
        delete oldHeightfieldNode.extensions.MOZ_hubs_components.heightfield;
      } else if (oldHeightfieldNode.extensions.HUBS_components) {
        delete oldHeightfieldNode.extensions.HUBS_components.heightfield;
      }
    }
  }
}

export async function loadGLTF(
  src,
  contentType,
  preferredTechnique,
  onProgress,
  jsonPreprocessor,
  texturedLightSystem,
  shouldComputeStats
) {
  let fileMap;

  if (!gltfLoader) {
    throw new Error(
      "gltf-model-plus::loadGLTF - Unexpected empty gltfLoader it should be initialized by gltf-model-plus"
    );
  }

  const loadedData = await new Promise((resolve, reject) =>
    gltfLoader.load(src, resolve, onProgress, reject, shouldComputeStats)
  );
  const parser = loadedData.parser;

  if (jsonPreprocessor) {
    parser.json = jsonPreprocessor(parser.json);
  }

  let version = 0;
  if (
    parser.json.extensions &&
    parser.json.extensions.MOZ_hubs_components &&
    parser.json.extensions.MOZ_hubs_components.hasOwnProperty("version")
  ) {
    version = parser.json.extensions.MOZ_hubs_components.version;
  }
  runMigration(version, parser.json);

  const nodes = parser.json.nodes;
  if (nodes) {
    for (let i = 0; i < nodes.length; i++) {
      const node = nodes[i];

      if (!node.extras) {
        node.extras = {};
      }

      node.extras.gltfIndex = i;
    }
  }

  const gltf = loadedData;
  if (shouldComputeStats) {
    gltf.stats = computeStats(parser, loadedData.scene);
  }
  gltf.scene.traverse(object => {
    // GLTFLoader sets matrixAutoUpdate on animated objects, we want to keep the defaults
    // New three.js update (0.14x->0.157) has changed the variable name
    object.matrixAutoUpdate = THREE.Object3D.DEFAULT_MATRIX_AUTO_UPDATE;

    /** New three.js update (0.14x->0.157) adds a method for computing bounding box on Skinned Mesh
     This method is messing with our bounding box computation and is run only if the bounding box is defined
     We cannot set bouding box to undefined because of aframe body attribute that does not take undefined 
     Bugs were:
     - rescale box not properly computed
     - rescaling object were not properly handled because of bbox wrong computation
     **/
    if (object instanceof THREE.SkinnedMesh) {
      object.computeBoundingBox = function () {  //do same logic as if bounding box was undefined https://github.com/mrdoob/three.js/blob/d04539a76736ff500cae883d6a38b3dd8643c548/src/math/Box3.js#L205 
        if(object.geometry) {
          const box = new THREE.Box3().makeEmpty();
          box.copy(object.geometry.boundingBox);
          object.boundingBox = box;
        }
      };
    }

    if (!texturedLightSystem || !isNightModeEnabled()) {
      object.material = mapMaterials(object, material => handleMaterial(material, preferredTechnique));
    } else {
      object.material = mapMaterials(object, material =>
        handleMaterialWithTextureLight(material, texturedLightSystem, preferredTechnique)
      );
    }
  });

  if (fileMap) {
    // The GLTF is now cached as a THREE object, we can get rid of the original blobs
    Object.keys(fileMap).forEach(URL.revokeObjectURL);
  }

  gltf.scene.animations = gltf.animations;
  return gltf;
}

export async function loadModel(
  src,
  contentType = null,
  useCache = false,
  jsonPreprocessor = null,
  texturedLightSystem,
  shouldComputeStats
) {
  const preferredTechnique =
    getCurrentSettings().quality === QUALITY_SETTINGS.LOW ? "KHR_materials_unlit" : "pbrMetallicRoughness";

  if (useCache) {
    if (gltfCache.has(src)) {
      gltfCache.retain(src);
      if (gltfCache.get(src) && !hasStats(gltfCache.get(src).gltf) && shouldComputeStats) {
        gltfCache.release(src);
        return loadModel(src, contentType, useCache, jsonPreprocessor, texturedLightSystem, shouldComputeStats);
      } else {
        return cloneGltf(gltfCache.get(src).gltf);
      }
    } else {
      if (inflightGltfsWithCache.has(src)) {
        const gltf = await inflightGltfsWithCache.get(src);
        gltfCache.retain(src);
        return cloneGltf(gltf);
      } else if (inflightGltfs.has(src) && !shouldComputeStats) {
        const gltf = await inflightGltfs.get(src);
        gltfCache.retain(src);
        return cloneGltf(gltf);
      } else {
        const promise = loadGLTF(
          src,
          contentType,
          preferredTechnique,
          null,
          jsonPreprocessor,
          texturedLightSystem,
          shouldComputeStats
        );
        let inflightsMap;
        if (shouldComputeStats) {
          inflightsMap = inflightGltfsWithCache;
        } else {
          inflightsMap = inflightGltfs;
        }
        inflightsMap.set(src, promise);
        const gltf = await promise;
        inflightsMap.delete(src);
        gltfCache.set(src, gltf);
        return cloneGltf(gltf);
      }
    }
  } else {
    return loadGLTF(
      src,
      contentType,
      preferredTechnique,
      null,
      jsonPreprocessor,
      texturedLightSystem,
      shouldComputeStats
    );
  }
}

function resolveAsset(src) {
  // If the src attribute is a selector, get the url from the asset item.
  if (src && src.charAt(0) === "#") {
    const assetEl = document.getElementById(src.substring(1));
    if (assetEl) {
      return assetEl.getAttribute("src");
    }
  }
  return src;
}

/**
 * Loads a GLTF model, optionally recursively "inflates" the child nodes of a model into a-entities and sets
 * whitelisted components on them if defined in the node's extras.
 * @namespace gltf
 * @component gltf-model-plus
 */
AFRAME.registerComponent("gltf-model-plus", {
  schema: {
    src: { type: "string" },
    contentType: { type: "string" },
    useCache: { default: true },
    inflate: { default: false },
    batch: { default: false },
    shouldComputeStats: { default: false },
    modelToWorldScale: { type: "number", default: 1 }
  },

  init() {
    // This can be set externally if a consumer wants to do some node preprocssing.
    this.jsonPreprocessor = null;
    this.stats = null;
    initializeGLTFLoader(this.el.sceneEl.renderer);
    this.loadTemplates();
  },

  update() {
    this.applySrc(resolveAsset(this.data.src), this.data.contentType);
  },

  remove() {
    const playerCollisionSystem = AFRAME.scenes[0].systems["player-collision-system"];
    this.el.object3D.traverse(x => {
      if (x.name.includes(COLLIDER_PREFIX) && playerCollisionSystem) {
        playerCollisionSystem.unregisterCollider(x);
      }
    });
    if (this.data.batch && this.model) {
      this.el.sceneEl.systems["hubs-systems"].batchManagerSystem.removeObject(this.el.object3DMap.mesh);
    }
    const src = resolveAsset(this.data.src);
    if (src) {
      gltfCache.release(src);
    }
  },

  loadTemplates() {
    this.templates = {};
    this.el.querySelectorAll(":scope > template").forEach(templateEl => {
      const root = document.importNode(templateEl.firstElementChild || templateEl.content.firstElementChild, true);
      this.templates[templateEl.getAttribute("data-name")] = root;
    });
  },

  async applySrc(src, contentType) {
    try {
      if (src === this.lastSrc) return;

      const lastSrc = this.lastSrc;
      this.lastSrc = src;

      if (!src) {
        if (this.inflatedEl) {
          console.warn("gltf-model-plus set to an empty source, unloading inflated model.");
          this.disposeLastInflatedEl();
        }
        return;
      }

      this.el.emit("model-loading");
      const gltf = await loadModel(
        src,
        contentType,
        this.data.useCache,
        this.jsonPreprocessor,
        this.el.sceneEl.systems[TEXTURE_LIGHT_SYSTEM_NAME],
        this.data.shouldComputeStats
      );

      // If we started loading something else already
      // TODO: there should be a way to cancel loading instead
      if (src != this.lastSrc) return;

      // If we had inflated something already before, clean that up
      this.disposeLastInflatedEl();

      this.model = gltf.scene || gltf.scenes[0];
      this.stats = gltf.stats;

      if (this.data.batch) {
        this.el.sceneEl.systems["hubs-systems"].batchManagerSystem.addObject(this.model);
      }
      if (gltf.animations.length > 0) {
        this.el.setAttribute("animated-mesh-bvh", {});
        this.el.setAttribute("animation-mixer", {});
        this.el.components["animation-mixer"].initMixer(this.model.animations);
      } else {
        generateMeshBVH(this.model);
      }
      const indexToEntityMap = {};

      let object3DToSet = this.model;
      if (
        this.data.inflate &&
        (this.inflatedEl = inflateEntities(
          indexToEntityMap,
          this.model,
          this.templates,
          true,
          this.data.modelToWorldScale
        ))
      ) {
        this.el.appendChild(this.inflatedEl);

        object3DToSet = this.inflatedEl.object3D;
        object3DToSet.visible = false;

        // TODO: Still don't fully understand the lifecycle here and how it differs between browsers, we should dig in more
        // Wait one tick for the appended custom elements to be connected before attaching templates
        await nextTick();
        if (src != this.lastSrc) return; // TODO: there must be a nicer pattern for this

        await inflateComponents(this.inflatedEl, indexToEntityMap);
        if (!this.inflatedEl.querySelector("[nav-mesh]")) {
          tryInstanciateWondaNavMesh(this.inflatedEl);
        }
        for (const name in this.templates) {
          attachTemplate(this.el, name, this.templates[name]);
        }
      }

      // The call to setObject3D below recursively clobbers any `el` backreferences to entities
      // in the entire inflated entity graph to point to `object3DToSet`.
      //
      // We don't want those overwritten, since lots of code assumes `object3d.el` points to the relevant
      // A-Frame entity for that three.js object, so we back them up and re-wire them here. If we didn't do
      // this, all the `el` properties on these object3ds would point to the `object3DToSet` which is either
      // the model or the root GLTF inflated entity.
      const rewires = [];

      object3DToSet.traverse(o => {
        const el = o.el;
        if (el) rewires.push(() => (o.el = el));
      });

      const environmentMapComponent = this.el.sceneEl.components["environment-map"];

      if (environmentMapComponent) {
        environmentMapComponent.applyEnvironmentMap(object3DToSet);
      }

      if (lastSrc) {
        gltfCache.release(lastSrc);
      }
      this.el.setObject3D("mesh", object3DToSet);

      rewires.forEach(f => f());

      object3DToSet.visible = true;
      instanciatePlayerCollider(this.el.object3D);
      this.el.emit("model-loaded", { format: "gltf", model: this.model });
    } catch (e) {
      gltfCache.release(src);
      console.error("Failed to load glTF model", e, this);
      this.el.emit("model-error", { format: "gltf", src });
    }
  },

  disposeLastInflatedEl() {
    if (this.inflatedEl) {
      this.inflatedEl.parentNode.removeChild(this.inflatedEl);
      const playerCollisionSystem = AFRAME.scenes[0].systems["player-collision-system"];
      this.inflatedEl.object3D.traverse(x => {
        if (x.name.includes(COLLIDER_PREFIX) && playerCollisionSystem) {
          playerCollisionSystem.unregisterCollider(x);
        }
        if (x.material && x.material.dispose) {
          x.material.dispose();
        }

        if (x.geometry) {
          if (x.geometry.dispose) {
            x.geometry.dispose();
          }

          x.geometry.boundsTree = null;
        }
      });

      delete this.inflatedEl;

      this.el.removeAttribute("animation-mixer");
    }
  }
});

//@From hubs - 1ee9829710bd581548a98d78db39f4e73013c3f6
class GLTFHubsLightMapExtension {
  constructor(parser) {
    this.parser = parser;
    this.name = "MOZ_lightmap";
  }

  // @TODO: Ideally we should use extendMaterialParams hook.
  //        But the current official glTF loader doesn't fire extendMaterialParams
  //        hook for unlit and specular-glossiness materials.
  //        So using loadMaterial hook as workaround so far.
  //        Cons is loadMaterial hook is fired as _invokeOne so
  //        if other plugins defining loadMaterial is registered
  //        there is a chance that this light map extension handler isn't called.
  //        The glTF loader should be updated to remove the limitation.
  loadMaterial(materialIndex) {
    const parser = this.parser;
    const json = parser.json;
    const materialDef = json.materials[materialIndex];

    if (!materialDef.extensions || !materialDef.extensions[this.name]) {
      return null;
    }

    const extensionDef = materialDef.extensions[this.name];

    const pending = [];

    pending.push(parser.loadMaterial(materialIndex));
    pending.push(parser.getDependency("texture", extensionDef.index));

    return Promise.all(pending).then(results => {
      const material = results[0];
      const lightMap = results[1];
      material.lightMap = lightMap;
      material.lightMapIntensity = extensionDef.intensity !== undefined ? extensionDef.intensity : 1;
      // New three.js update (0.14x->0.157) changed the lightmap handling which does not always default to uv2
      // To be compatible with previous version we need to use channel = 1 for uv2
      material.lightMap.channel = 1;
      return material;
    });
  }
}
