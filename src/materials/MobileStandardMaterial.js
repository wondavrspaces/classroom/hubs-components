export default class MobileStandardMaterial extends THREE.MeshBasicMaterial {
  static fromMeshStandardMaterial(source) {
    const material = new MobileStandardMaterial();
    material.copy(source);
    return material;
  }

  constructor(material) {
    super(material);
  }
}
