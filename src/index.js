import "super-hands";

import { patchWebGLRenderingContext } from "./utils/webgl";
import "./utils/audio-context-fix";
import "./utils/threejs-world-update";

import "./systems/audio-system";
import "./systems/personal-space-bubble";
import "./systems/nav";
import "./systems/exit-on-blur";
import "./systems/userinput/userinput";
import "./systems/userinput/userinput-debug";
import "./systems/modal";

import "./components/scene-components";

patchWebGLRenderingContext();

export { keyboardMouseUserBindings } from "./systems/userinput/bindings/keyboard-mouse-user";
export { configure, getCurrentSettings } from "./utils/settings";
export { QUALITY_SETTINGS, EVENTS } from "./utils/constants";
export { paths } from "./systems/userinput/paths";
export { setMatrixWorld } from "./utils/three-utils";
export * from "./systems/audio-system";
export { loadGLTF, initializeGLTFLoader, getGLTFLoader } from "./components/gltf-model-plus";
export * from "./materials/MobileStandardMaterial";

import "./systems/player-collider-system";
import "./components/player-collision-rig";
