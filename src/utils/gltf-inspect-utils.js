function inspectObject3D(object3D, meshesStats, materialsStats) {
  if (object3D.isMesh || object3D.isSkinnedMesh) {
    meshesStats.count++;
    //If index is here, we know that each index will represent a point and a triangle will be formed by 3 consecutives vertices
    //This computation method should work with TRIANGLE_STRIP and TRIANGLE_FAN as they are cast to TRIANGLE with toTrianglesDrawMode in GLTFLoader
    if (object3D.geometry.index) {
      meshesStats.triangles += object3D.geometry.index.count / 3;
      meshesStats.vertices += object3D.geometry.index.count;
    } else {
      //For buffer attributes, the count is returning the array's length divided by item size
      //For position, it will count the number of vector with 3 components for example which corresponds to a vertex.
      meshesStats.triangles += object3D.geometry.attributes.position.count / 3;
      meshesStats.vertices += object3D.geometry.attributes.position.count;
    }
  }
  if (object3D.isPoints) {
    meshesStats.count++;
    meshesStats.vertices += object3D.geometry.attributes.position.count;
  }
  //Getting material data
  if (object3D.material) {
    materialsStats.count++;
  }
}

function inspectTextures(textureInfos, textureStats) {
  if (!textureInfos) return;
  for (const textureKey in textureInfos) {
    const texture = textureInfos[textureKey];
    textureStats.count++;
    textureStats.gpuMinimumTotalSize += texture.gpuMinimumTotalSize;
    if (texture.width > textureStats.maxWidthResolution) {
      textureStats.maxWidthResolution = texture.width;
    }
    if (texture.height > textureStats.maxHeightResolution) {
      textureStats.maxHeightResolution = texture.height;
    }
  }
}

export function computeStats(parser, scene) {
  const meshesStats = {
    count: 0,
    vertices: 0,
    triangles: 0
  };
  const materialsStats = {
    count: 0
  };
  const textureStats = {
    count: 0,
    gpuMinimumTotalSize: 0,
    maxWidthResolution: 0,
    maxHeightResolution: 0
  };
  const stats = {
    jsonSize: parser.stats.jsonSize,
    fileSize: parser.stats.fileSize,
    texturesInfo: parser.stats.textureInfo,
    nodeCount: 0,
    meshes: meshesStats,
    materials: materialsStats,
    textures: textureStats
  };
  scene.traverse(object3D => {
    stats.nodeCount++;
    inspectObject3D(object3D, meshesStats, materialsStats);
  });

  inspectTextures(parser.stats.textureInfo, textureStats);
  return stats;
}
