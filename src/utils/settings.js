import { QUALITY_SETTINGS } from "./constants";

const defaultSettings = {
  quality: QUALITY_SETTINGS.HIGH,
  logger: console,
  isNightModeEnabled: false
};

const currentSettings = defaultSettings;
export function configure(settings = defaultSettings) {
  const qualitySetting = getQualitySetting(settings);
  const loggerSetting = getLogger(settings);
  const isNightModeEnabled = settings.isNightModeEnabled || false;
  currentSettings.quality = qualitySetting;
  currentSettings.logger = loggerSetting;
  currentSettings.isNightModeEnabled = isNightModeEnabled;
}

export function isNightModeEnabled() {
  return currentSettings.isNightModeEnabled;
}

export function getCurrentSettings() {
  return currentSettings;
}

function getLogger(settings) {
  if (!settings.logger) {
    return defaultSettings.logger;
  }

  return settings.logger;
}

function getQualitySetting(settings) {
  if (!settings.quality) {
    return defaultSettings.quality;
  }
  if (!validateQualitySetting(settings.quality)) {
    throw new Error("configure-hubs-components::getQualitySettings: Not valid quality settings");
  }
  return settings.quality;
}

function validateQualitySetting(qualitySetting) {
  return Object.values(QUALITY_SETTINGS).indexOf(qualitySetting) !== -1;
}
