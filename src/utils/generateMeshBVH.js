// From hubs repository gltf-model-plus.js
import { MeshBVH, acceleratedRaycast, CENTER } from "three-mesh-bvh";
import { getCurrentSettings } from "./settings";

THREE.Mesh.prototype.raycast = acceleratedRaycast;
// New three.js update (0.14x->0.157) add a raycast method to SkinnedMesh which were previously inherited from mesh
// We have to override raycast to acceleratedRaycast on SkinnedMesh
THREE.SkinnedMesh.prototype.raycast = acceleratedRaycast;

export function generateMeshBVH(object3D) {
  object3D.traverse(obj => {
    // note that we might already have a bounds tree if this was a clone of an object with one
    const hasBufferGeometry = obj.isMesh && obj.geometry.isBufferGeometry;
    const hasBoundsTree = hasBufferGeometry && obj.geometry.boundsTree;
    if (hasBufferGeometry && !hasBoundsTree && obj.geometry.attributes.position) {
      const geo = obj.geometry;

      if (geo.index && geo.index.isInterleavedBufferAttribute) {
        getCurrentSettings().logger.warn(
          "generateMeshBVH::generateMeshBVH - Skipping generaton of MeshBVH for interleaved geoemtry as it is not supported"
        );
        return;
      }

      const triCount = geo.index ? geo.index.count / 3 : geo.attributes.position.count / 3;
      // only bother using memory and time making a BVH if there are a reasonable number of tris,
      // and if there are too many it's too painful and large to tolerate doing it (at least until
      // we put this in a web worker)

      if (triCount > 1000 && triCount < 1000000) {
        // note that bounds tree construction creates an index as a side effect if one doesn't already exist
        geo.boundsTree = new MeshBVH(obj.geometry, { strategy: CENTER, maxDepth: 30 });
      }
    }
  });
}
