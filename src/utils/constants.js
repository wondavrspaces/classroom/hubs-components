export const QUALITY_SETTINGS = {
  HIGH: "high",
  LOW: "low"
};

export const EVENTS = {
  CLICK_ON_INTERACTABLE: "clickOnInteractable"
};
