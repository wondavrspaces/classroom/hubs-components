//This tools is extracted from https://github.com/donmccurdy/glTF-Transform
//The basic image utils and the ktx2 image utils has been copied and smally modified to fit our needs
import { read as readKTX, KHR_DF_MODEL_ETC1S, KDF_DF_MODEL_UASTC } from "ktx-parse";

export async function getMemoryMinimumSize(imageURL, imageMimeType, imageWidth, imageHeight) {
  if (imageMimeType === "image/jpeg" || imageMimeType === "image/png") {
    let uncompressedBytes = 0;
    const channels = 4; // See https://github.com/donmccurdy/glTF-Transform/issues/151.
    const resolution = [imageWidth, imageHeight];
    if (!resolution) return null;

    while (resolution[0] > 1 || resolution[1] > 1) {
      uncompressedBytes += resolution[0] * resolution[1] * channels;
      resolution[0] = Math.max(Math.floor(resolution[0] / 2), 1);
      resolution[1] = Math.max(Math.floor(resolution[1] / 2), 1);
    }
    uncompressedBytes += 1 * 1 * channels;
    return uncompressedBytes;
  } else if (imageMimeType === "image/ktx2") {
    let buffer = await fetch(imageURL).then(r => r.arrayBuffer());
    const array = new Uint8Array(buffer);
    const container = readKTX(array);
    const channels = getChannels(container);
    const hasAlpha = channels > 3;

    let uncompressedBytes = 0;
    for (let i = 0; i < container.levels.length; i++) {
      const level = container.levels[i];

      // Use level.uncompressedByteLength for UASTC; for ETC1S it's 0.
      if (level.uncompressedByteLength) {
        uncompressedBytes += level.uncompressedByteLength;
      } else {
        const levelWidth = Math.max(1, Math.floor(container.pixelWidth / Math.pow(2, i)));
        const levelHeight = Math.max(1, Math.floor(container.pixelHeight / Math.pow(2, i)));
        const blockSize = hasAlpha ? 16 : 8;
        uncompressedBytes += (levelWidth / 4) * (levelHeight / 4) * blockSize;
      }
    }
    return uncompressedBytes;
  }
}

function getChannels(ktxContainer) {
  const dfd = ktxContainer.dataFormatDescriptor[0];
  if (dfd.colorModel === KHR_DF_MODEL_ETC1S) {
    return dfd.samples.length === 2 && (dfd.samples[1].channelType & 0xf) === 15 ? 4 : 3;
  } else if (dfd.colorModel === KDF_DF_MODEL_UASTC) {
    return (dfd.samples[0].channelType & 0xf) === 3 ? 4 : 3;
  }
}