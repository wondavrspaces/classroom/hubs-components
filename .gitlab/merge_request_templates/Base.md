# Summary

[WVR-1110](https://wondavr.atlassian.net/browse/WVR-1110): 
When we update the name of a section or article in the help center,
all old links are not automatically redirected to the new one, resulted in a broken link.

## Functional overview

The issue has been caused by changed on documentation with the localization of articles. 
You can already test completed functionalities.

TODO list:
- [ ] a task list item
- [ ] list syntax required
- [ ] normal **formatting**, @mentions, #1234 refs
- [ ] incomplete
- [x] completed feature


## Technical overview

Refreshing link with new version fixed the issue. Note that following has been done.

# WARNING 

## Functional warning

New help buttons creation have been merged with this bug fix. 

## Technical warning 

Not much

# TODO

## Handling xxx

Handling xxx has not been implemented. Could be done..