const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  context: path.resolve(__dirname, "src"),
  entry: {
    components: ["./index.js"]
  },
  externals: {
    "three": "three",
    "@wondavrspaces/texture-light": "@wondavrspaces/texture-light"
  },
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "[name].bundle.js",
    library: "hubs-components",
    libraryTarget: "umd"
  },
  mode: "development",
  devtool: "eval",
  watch: false,
  watchOptions: {
    ignored: /node_modules/
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [path.resolve(__dirname, "src"), path.resolve(__dirname, "node_modules/@wondavrspaces/hubs/src/")],
        loader: "babel-loader",
        query: {
          plugins: ["transform-class-properties", "transform-object-rest-spread"]
        }
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: "css-loader",
              options: {
                name: "[path][name]-[hash].[ext]",
                minimize: process.env.NODE_ENV === "production",
                localIdentName: "[name]__[local]__[hash:base64:5]",
                camelCase: true
              }
            },
            "sass-loader"
          ]
        })
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: {
            loader: "css-loader",
            options: {
              name: "[path][name]-[hash].[ext]",
              minimize: process.env.NODE_ENV === "production",
              localIdentName: "[name]__[local]__[hash:base64:5]",
              camelCase: true
            }
          }
        })
      },
      {
        test: /\.(glsl)$/,
        use: { loader: "raw-loader" }
      },
      {
        test: /\.(png|jpg|gif|glb|ogg|mp3|mp4|wav|woff2|svg|webm)$/,
        use: {
          loader: "file-loader",
          options: {
            // move required assets to output dir and add a hash for cache busting
            name: "[path][name]-[hash].[ext]",
            // Make asset paths relative to /src
            context: path.join(__dirname, "src")
          }
        }
      },
      {
        test: /\.worker\.js$/,
        loader: "worker-loader",
        options: {
          name: "assets/js/[name].js",
          publicPath: "/",
          inline: true
        }
      }
    ]
  },
  plugins: [
    // Extract required css and add a content hash.
    new ExtractTextPlugin({
      filename: "assets/stylesheets/[name]-[contenthash].css",
      disable: process.env.NODE_ENV !== "production"
    })
  ]
};
